package mobile.jason.audiobookclub.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import mobile.jason.audiobookclub.Fragments.BookDetailsFragment;
import mobile.jason.audiobookclub.Fragments.BrowseBooksGridFragment;
import mobile.jason.audiobookclub.Fragments.ChapterFragment;
import mobile.jason.audiobookclub.Fragments.DashboardFragment;
import mobile.jason.audiobookclub.Fragments.MyBooksFragment;
import mobile.jason.audiobookclub.Fragments.MyBooksGridFragment;
import mobile.jason.audiobookclub.Fragments.ProfileFragment;
import mobile.jason.audiobookclub.Fragments.RecordTrailerFragment;
import mobile.jason.audiobookclub.Fragments.TrailerListFragment;
import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.DrawerAdapter;
import mobile.jason.audiobookclub.database.CommentsSQLiteHelper;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.entities.Chapter;
import mobile.jason.audiobookclub.entities.Comment;
import mobile.jason.audiobookclub.entities.LVoxBookWrapper;
import mobile.jason.audiobookclub.entities.UserProfile;
import mobile.jason.audiobookclub.managers.DownloadManager;
import mobile.jason.audiobookclub.managers.FilesManager;
import mobile.jason.audiobookclub.managers.MediaManager;
import mobile.jason.audiobookclub.managers.ProgressManager;
import mobile.jason.audiobookclub.views.RoundImageView;

public class MainActivity extends ActionBarActivity implements
        DashboardFragment.OnFragmentInteractionListener,
        MyBooksFragment.OnFragmentInteractionListener,
        MyBooksGridFragment.OnFragmentInteractionListener,
        BookDetailsFragment.OnFragmentInteractionListener,
        BrowseBooksGridFragment.OnFragmentInteractionListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LinearLayout mDrawer;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private SignInButton mSignInButton;
    private RelativeLayout mProfileLayout;
    private TextView mProfileName;
    private ImageView mProfileImage;
    private ProgressBar mProgressBar;
    private GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    private ImageView mProfileDropdown;
    ActionBar mActionBar;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mDrawerItems;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private boolean isSingedIn;
    private String personName;
    private String personPhotoUrl;
    private String personGooglePlusProfile;
    private String email;
    private CommentsSQLiteHelper mHelper;
    private static final int RC_SIGN_IN = 0;

    public static final String INTENT_OPEN_LISTEN_TO_TRAILER = "INTENT_OPEN_LISTEN_TO_TRAILER";
    public static final String INTENT_OPEN_RECORD_TRAILER = "INTENT_OPEN_RECORD_TRAILER";

    private ListenToTrailerBroadcastReceiver _listenToTrailerBroadcastReceiver;
    private RecordTrailerBroadcastReceiver _recordTrailerBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        moveDrawerToTop();
        mTitle = mDrawerTitle = getTitle();
        mDrawerItems = getResources().getStringArray(R.array.drawer_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer = (LinearLayout) findViewById(R.id.left_drawer);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
        mProfileLayout = (RelativeLayout) findViewById(R.id.profile_layout);
        mProfileName = (TextView) findViewById(R.id.profile_name);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProfileDropdown = (ImageView) findViewById(R.id.profile_dropdown);
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();
        //mRevokeButton = (Button) findViewById(R.id.revoke_button);
        mHelper = new CommentsSQLiteHelper(this);
        _listenToTrailerBroadcastReceiver = new ListenToTrailerBroadcastReceiver();
        _recordTrailerBroadcastReceiver = new RecordTrailerBroadcastReceiver();

        mDrawerList.setAdapter(new DrawerAdapter(this, mDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mSignInButton.setOnClickListener(new SignInButtonClickListener());
        mProfileDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), mProfileDropdown);
                popupMenu.getMenuInflater().inflate(R.menu.profile_drop_down_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_profile_logout:
                                signOutFromGPlus();
                                break;
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTintToUpIcon();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }

        FilesManager.getInstance().getMyBooks(this);
        DownloadManager.getInstance().loadLatestBooks();

        CommentsSQLiteHelper helper = new CommentsSQLiteHelper(this);
        helper.dropTable();
        /*helper.addComment(new Comment("changeling_1212_librivox", 0,  "The ending of the Act was quite unexpected. I can't believe Elizabeth could do something like that. I'm not quite sure what her motivation for killing Sean is."));
        helper.addComment(new Comment("changeling_1212_librivox", 0, "The meaning behind the market scene was pretty vague. Can someone explain it to me?"));
        helper.addComment(new Comment("changeling_1212_librivox", 0, "Sean Bean dies again. Expected/10"));

        helper.addComment(new Comment("changeling_1212_librivox", 1, "His character was so central to Act I, his disappearance in Act II is quite mysterious."));
        helper.addComment(new Comment("changeling_1212_librivox", 1, "The way she stood her ground against the ministers reveals her powerful character. Her place as queen is irrefutable after the throne room scene. She should be a role model for all females."));
        helper.addComment(new Comment("changeling_1212_librivox", 1, "When the conclusion expresses what ought to be, based only on actually what is more natural.  This is very common, and most people never see the problem with these kinds of assertions due to accepted social and moral norms.  This bypasses reason and we fail to ask why something that is, ought to be that way"));
        helper.addComment(new Comment("changeling_1212_librivox", 1, "Awesome"));*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        //boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerToggle.syncState();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        /*
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        */
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    //From Dashboard Fragment && MyBooksGridFragment
    @Override
    public void onShowBookDetail(Audiobook book) {
        BookDetailsFragment frag = BookDetailsFragment.newInstance(book);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //From BrowseBooksGridFragment
    @Override
    public void onShowBookDetail(LVoxBookWrapper book) {
        BookDetailsFragment frag = BookDetailsFragment.newInstance(book);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //From BookDetailsFragment
    @Override
    public void onChapterSelected(String urlTitle, ArrayList<Chapter> chapters, Bitmap coverArt, int chapterNumber) {
        ChapterFragment frag = ChapterFragment.newInstance(urlTitle, chapters, coverArt, chapterNumber);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void moveDrawerToTop() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout drawer = (DrawerLayout) inflater.inflate(R.layout.drawer, null); // "null" is important.
        drawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        View child = decor.getChildAt(0);
        decor.removeView(child);
        LinearLayout container = (LinearLayout) drawer.findViewById(R.id.content_frame); // This is the container we defined just now.
        container.addView(child);
        drawer.findViewById(R.id.left_drawer).setPadding(0, getStatusBarHeight(), 0, 0);

        decor.addView(drawer);
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setTintToUpIcon() {
        int upId = getResources().getIdentifier("android:id/up", null, null);
        if(upId == 0) return ;

        View view = getWindow().getDecorView().findViewById(upId);
        if(!(view instanceof ImageView)) return ;

        ImageView imageView = (ImageView) view;
        imageView.setColorFilter(Color.argb(222 ,255, 255, 255));
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
        //Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
        getProfileInformation();
        isSingedIn = true;
        updateButtonText(true);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        isSingedIn = false;
        updateButtonText(false);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = connectionResult;
            if (mSignInClicked) {
                resolveSignInError();
            }
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new DashboardFragment();
                break;
            case 1:
                fragment = new MyBooksGridFragment();
                break;
            case 2:
                fragment = new BrowseBooksGridFragment();
                break;
            case 3:
                fragment = new ProfileFragment();
                break;
            default:
                fragment = new DashboardFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.main_content, fragment).commit();

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawer);
    }

    private class SignInButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if(!isSingedIn) {
                signInWithGPlus();
            } else {
                signOutFromGPlus();
            }
        }
    }

    private class RevokeButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            revokeGPlusAccess();
        }
    }

    private void updateButtonText(boolean isSignedIn) {
        for (int i = 0; i < mSignInButton.getChildCount(); i++) {
            View v = mSignInButton.getChildAt(i);
            if (v instanceof TextView) {
                //TextView mTextView = (TextView) v;
                if(isSignedIn) {
                    //mTextView.setText("SIGN OUT");
                    mSignInButton.setVisibility(View.GONE);
                    mProfileLayout.setVisibility(View.VISIBLE);
                } else {
                    //mTextView.setText("SIGN IN");
                    mSignInButton.setVisibility(View.VISIBLE);
                    mProfileLayout.setVisibility(View.GONE);
                }
                return;
            }
        }
    }

    private void signInWithGPlus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private void signOutFromGPlus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            isSingedIn = false;
            updateButtonText(false);
        }
    }

    private void revokeGPlusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status arg0) {
                    mGoogleApiClient.connect();
                    isSingedIn = false;
                    updateButtonText(false);
                }
            });
        }
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                personName = currentPerson.getDisplayName();
                personPhotoUrl = currentPerson.getImage().getUrl();
                personGooglePlusProfile = currentPerson.getUrl();
                email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                saveEmailToPref(email);

                mProfileName.setText(personName);

                personPhotoUrl = personPhotoUrl.substring(0,personPhotoUrl.length() - 2)+ 200;
                new LoadProfileImage(mProfileImage).execute(personPhotoUrl);
            } else {
                Toast.makeText(getApplicationContext(), "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveEmailToPref(String email) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        pref.edit().putString("PREF_EMAIL", email).commit();
        pref.edit().putBoolean("SIGN_IN", true).commit();
    }

    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap photo = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                photo = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return photo;
        }

        protected void onPostExecute(Bitmap result) {
            RoundImageView roundImageView = new RoundImageView(MainActivity.this);
            result = roundImageView.getCroppedBitmap(result, 200);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            result.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            mHelper.addUser(new UserProfile(personName, byteArray, personGooglePlusProfile, email));
            bmImage.setImageBitmap(result);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.main_content);
        if(f instanceof ChapterFragment) {
            MediaManager.getInstance().stop();
        }
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(_listenToTrailerBroadcastReceiver, new IntentFilter(INTENT_OPEN_LISTEN_TO_TRAILER));
        registerReceiver(_recordTrailerBroadcastReceiver, new IntentFilter(INTENT_OPEN_RECORD_TRAILER));
    }

    @Override
    protected void onPause() {
        unregisterReceiver(_listenToTrailerBroadcastReceiver);
        unregisterReceiver(_recordTrailerBroadcastReceiver);
        super.onPause();
    }

    public class ListenToTrailerBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openTrailerListFragment(intent);
        }
    }

    private void openTrailerListFragment(Intent intent) {
        String urlTitle = intent.getStringExtra("URL_TITLE");
        Bundle bundle = new Bundle();
        bundle.putString("URL_TITLE", urlTitle);
        TrailerListFragment frag = new TrailerListFragment();
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_content, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class RecordTrailerBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openRecordTrailerFragment(intent);
        }
    }

    private void openRecordTrailerFragment(Intent intent) {
        if(getFlagFromPref()){
            String urlTitle = intent.getStringExtra("URL_TITLE");
            Bundle bundle = new Bundle();
            bundle.putString("URL_TITLE", urlTitle);
            RecordTrailerFragment frag = new RecordTrailerFragment();
            frag.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_content, frag);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            Toast.makeText(this, "Please login first!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean getFlagFromPref() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        return pref.getBoolean("SIGN_IN", false);
    }
}