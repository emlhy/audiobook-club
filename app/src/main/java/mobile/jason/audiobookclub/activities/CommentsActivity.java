package mobile.jason.audiobookclub.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;
import android.view.Window;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.Fragments.AudioClipsFragment;
import mobile.jason.audiobookclub.Fragments.AudioRecorderFragment;
import mobile.jason.audiobookclub.Fragments.CommentsFragment;
import mobile.jason.audiobookclub.Fragments.QAFragment;
import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.ui.TitleIndicator;

/**
 * Created by Demon Pilot on 2015/3/15.
 */
public class CommentsActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {

    public static final int FRAGMENT_ONE = 0;
    public static final int FRAGMENT_TWO = 1;
    public static final int FRAGMENT_THREE = 2;

    public static final String EXTRA_TAB = "tab";
    protected int mCurrentTab = 0;
    protected int mLastTab = -1;

    protected ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
    protected MyAdapter myAdapter = null;
    protected ViewPager mPager;
    protected TitleIndicator mIndicator;

    private int fragmentNumber;
    private String mUrlTitle;
    private int chapterNumber;
    public static final String INTENT_OPEN_AUDIO_RECORDER = "INTENT_OPEN_AUDIO_RECORDER";
    public static final String INTENT_OPEN_AUDIO_CLIPS = "INTENT_OPEN_AUDIO_CLIPS";
    private AudioRecorderBroadcastReceiver _audioRecorderBroadcastReceiver;
    private AudioClipsBroadcastReceiver _audioClipsBroadcastReceiver;

    public class MyAdapter extends FragmentPagerAdapter {
        ArrayList<TabInfo> tabs = null;
        Context context = null;

        public MyAdapter(Context context, FragmentManager fm, ArrayList<TabInfo> tabs) {
            super(fm);
            this.tabs = tabs;
            this.context = context;
        }

        @Override
        public Fragment getItem(int pos) {
            Fragment fragment = null;
            if (tabs != null && pos < tabs.size()) {
                TabInfo tab = tabs.get(pos);
                if (tab == null)
                    return null;
                fragment = tab.createFragment();
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            if (tabs != null && tabs.size() > 0)
                return tabs.size();
            return 0;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TabInfo tab = tabs.get(position);
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            tab.fragment = fragment;
            return fragment;
        }
    }

    public class AudioRecorderBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openAudioRecorderFragment();
        }

    }

    public class AudioClipsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            openAudioClipsFragment();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Intent intent=getIntent();
        fragmentNumber = intent.getIntExtra("FRAG_NUM", 0);

        setContentView(R.layout.titled_fragment_tab_activity);
        _audioRecorderBroadcastReceiver = new AudioRecorderBroadcastReceiver();
        _audioClipsBroadcastReceiver = new AudioClipsBroadcastReceiver();
        initViews();

        mPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.page_margin_width));
        mPager.setPageMarginDrawable(R.color.primary_color_orange);
    }

    @Override
    protected void onDestroy() {
        mTabs.clear();
        mTabs = null;
        myAdapter.notifyDataSetChanged();
        myAdapter = null;
        mPager.setAdapter(null);
        mPager = null;
        mIndicator = null;

        super.onDestroy();
    }

    private final void initViews() {
        mCurrentTab = supplyTabs(mTabs);
        Intent intent = getIntent();
        if (intent != null) {
            mCurrentTab = intent.getIntExtra(EXTRA_TAB, mCurrentTab);
        }
        myAdapter = new MyAdapter(this, getSupportFragmentManager(), mTabs);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(myAdapter);
        mPager.setOnPageChangeListener(this);
        mPager.setOffscreenPageLimit(mTabs.size());

        mIndicator = (TitleIndicator) findViewById(R.id.pager_indicator);
        mIndicator.init(mCurrentTab, mTabs, mPager);

        mPager.setCurrentItem(mCurrentTab);
        mLastTab = mCurrentTab;
    }

    public void addTabInfo(TabInfo tab) {
        mTabs.add(tab);
        myAdapter.notifyDataSetChanged();
    }

    public void addTabInfos(ArrayList<TabInfo> tabs) {
        mTabs.addAll(tabs);
        myAdapter.notifyDataSetChanged();
    }

    public class TabInfo implements Parcelable {

        private int id;
        private int icon;
        private String name = null;
        public boolean hasTips = false;
        public Fragment fragment = null;
        public boolean notifyChange = false;
        @SuppressWarnings("rawtypes")
        public Class fragmentClass = null;

        @SuppressWarnings("rawtypes")
        public TabInfo(int id, String name, Class clazz) {
            this(id, name, 0, clazz);
        }

        @SuppressWarnings("rawtypes")
        public TabInfo(int id, String name, boolean hasTips, Class clazz) {
            this(id, name, 0, clazz);
            this.hasTips = hasTips;
        }

        @SuppressWarnings("rawtypes")
        public TabInfo(int id, String name, int iconid, Class clazz) {
            super();

            this.name = name;
            this.id = id;
            icon = iconid;
            fragmentClass = clazz;
        }

        public TabInfo(Parcel p) {
            this.id = p.readInt();
            this.name = p.readString();
            this.icon = p.readInt();
            this.notifyChange = p.readInt() == 1;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setIcon(int iconid) {
            icon = iconid;
        }

        public int getIcon() {
            return icon;
        }

        @SuppressWarnings({ "rawtypes", "unchecked" })
        public Fragment createFragment() {
            if (fragment == null) {
                Constructor constructor;
                try {
                    constructor = fragmentClass.getConstructor(new Class[0]);
                    fragment = (Fragment) constructor.newInstance(new Object[0]);
                    Intent intent = CommentsActivity.this.getIntent();
                    fragment.setArguments(intent.getExtras());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return fragment;
        }

        public final Parcelable.Creator<TabInfo> CREATOR = new Parcelable.Creator<TabInfo>() {
            public TabInfo createFromParcel(Parcel p) {
                return new TabInfo(p);
            }

            public TabInfo[] newArray(int size) {
                return new TabInfo[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel p, int flags) {
            p.writeInt(id);
            p.writeString(name);
            p.writeInt(icon);
            p.writeInt(notifyChange ? 1 : 0);
        }

    }

    protected TabInfo getFragmentById(int tabId) {
        if (mTabs == null) return null;
        for (int index = 0, count = mTabs.size(); index < count; index++) {
            TabInfo tab = mTabs.get(index);
            if (tab.getId() == tabId) {
                return tab;
            }
        }
        return null;
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mIndicator.onScrolled((mPager.getWidth() + mPager.getPageMargin()) * position + positionOffsetPixels);
    }

    @Override
    public void onPageSelected(int position) {
        mIndicator.onSwitched(position);
        mCurrentTab = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            mLastTab = mCurrentTab;
        }
    }

    public void navigate(int tabId) {
        for (int index = 0, count = mTabs.size(); index < count; index++) {
            if (mTabs.get(index).getId() == tabId) {
                mPager.setCurrentItem(index);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(this.mPager.getCurrentItem() == 2 && this.mTabs.get(2).fragmentClass == AudioRecorderFragment.class){
            openAudioClipsFragment();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // for fix a known issue in support library
        // https://code.google.com/p/android/issues/detail?id=19917
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    protected int supplyTabs(List<TabInfo> tabs) {
        tabs.add(new TabInfo(FRAGMENT_ONE, getString(R.string.write_comment),
                CommentsFragment.class));
        tabs.add(new TabInfo(FRAGMENT_TWO, getString(R.string.ask_comment),
                QAFragment.class));
        tabs.add(new TabInfo(FRAGMENT_THREE, getString(R.string.record_comment),
                AudioClipsFragment.class));
        return fragmentNumber;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(_audioRecorderBroadcastReceiver);
        unregisterReceiver(_audioClipsBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(_audioRecorderBroadcastReceiver, new IntentFilter(INTENT_OPEN_AUDIO_RECORDER));
        registerReceiver(_audioClipsBroadcastReceiver, new IntentFilter(INTENT_OPEN_AUDIO_CLIPS));
    }

    private void openAudioRecorderFragment() {
        Fragment replace = myAdapter.getItem(2);
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(replace).commit();
        mTabs.remove(2);
        mTabs.add(new TabInfo(FRAGMENT_THREE, getString(R.string.record_comment),AudioRecorderFragment.class));
        myAdapter.notifyDataSetChanged();
    }

    private void openAudioClipsFragment() {
        Fragment replace = myAdapter.getItem(2);
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(replace).commit();
        mTabs.remove(2);
        mTabs.add(new TabInfo(FRAGMENT_THREE, getString(R.string.record_comment),AudioClipsFragment.class));
        myAdapter.notifyDataSetChanged();
    }


}