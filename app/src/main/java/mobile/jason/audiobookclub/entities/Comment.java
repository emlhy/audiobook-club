package mobile.jason.audiobookclub.entities;

/**
 * Created by Jason on 3/3/2015.
 */
public class Comment {
    private String _urlTitle;
    private int _chapterNumber;
    private String _commentBody;
    private String _userName;
    private byte[] _userImage;

    public Comment() {}
    public Comment(String urlTitle, int chapterNumber, String commentBody, String userName, byte[] userImage) {
        _urlTitle = urlTitle;
        _chapterNumber = chapterNumber;
        _commentBody = commentBody;
        _userName = userName;
        _userImage = userImage;
    }

    public String get_urlTitle() {
        return _urlTitle;
    }

    public void set_urlTitle(String _urlTitle) {
        this._urlTitle = _urlTitle;
    }

    public int get_chapterNumber() {
        return _chapterNumber;
    }

    public void set_chapterNumber(int _chapterNumber) {
        this._chapterNumber = _chapterNumber;
    }

    public String get_commentBody() {
        return _commentBody;
    }

    public void set_commentBody(String _commentBody) {
        this._commentBody = _commentBody;
    }

    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public byte[] get_userImage() {
        return _userImage;
    }

    public void set_userImage(byte[] _userImage) {
        this._userImage = _userImage;
    }
}
