package mobile.jason.audiobookclub.entities;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Jason on 3/2/2015.
 */
public class Audiobook {
    private int _id;
    private String _title;
    private String _description;
    private String _language;
    private String _urlTitle;
    private int _totalTimeSecs;
    private HashSet<String> _genres;
    private HashSet<String> _authors;
    private HashSet<String> _translators;
    private Bitmap _thumbnail;
    private Bitmap _coverArt;
    private ArrayList<Chapter> _chapters;

    public Audiobook() {
        _chapters = new ArrayList<Chapter>();
        _genres = new HashSet<String>();
        _authors = new HashSet<String>();
        _translators = new HashSet<String>();
    }

    public void add_chapter(Chapter chapter) {
        _chapters.add(chapter);
    }

    public void add_author(String author) {
        _authors.add(author);
    }

    public void add_genre(String genre) {
        _genres.add(genre);
    }

    public void add_translator(String translator) {
        _translators.add(translator);
    }

    public String get_author() {
        if (_authors.isEmpty()) {
            return "None";
        } else if (_authors.size() > 1) {
            return "Various";
        } else {
            return _authors.toArray()[0].toString();
        }
    }

    public Bitmap get_coverArt() {
        return _coverArt;
    }

    public void set_coverArt(Bitmap _coverArt) {
        this._coverArt = _coverArt;
    }

    public ArrayList<Chapter> get_chapters() {
        return _chapters;
    }

    public void set_chapters(ArrayList<Chapter> _chapters) {
        this._chapters = _chapters;
    }

    public Bitmap get_thumbnail() {
        return _thumbnail;
    }

    public void set_thumbnail(Bitmap _thumbnail) {
        this._thumbnail = _thumbnail;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_language() {
        return _language;
    }

    public void set_language(String _language) {
        this._language = _language;
    }

    public String get_urlTitle() {
        return _urlTitle;
    }

    public void set_urlTitle(String _urlTitle) {
        this._urlTitle = _urlTitle;
    }

    public int get_totalTimeSecs() {
        return _totalTimeSecs;
    }

    public void set_totalTimeSecs(int _totalTimeSecs) {
        this._totalTimeSecs = _totalTimeSecs;
    }

    public HashSet<String> get_genres() {
        return _genres;
    }

    public void set_genres(HashSet<String> _genres) {
        this._genres = _genres;
    }

    public HashSet<String> get_authors() {
        return _authors;
    }

    public void set_authors(HashSet<String> _authors) {
        this._authors = _authors;
    }

    public HashSet<String> get_translators() {
        return _translators;
    }

    public void set_translators(HashSet<String> _translators) {
        this._translators = _translators;
    }
}
