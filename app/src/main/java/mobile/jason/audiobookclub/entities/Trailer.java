package mobile.jason.audiobookclub.entities;

/**
 * Created by Demon Pilot on 2015/4/1.
 */
public class Trailer {
    private String _urlTitle;
    String _trailerName;
    byte[] _trailerByte;
    private String _userName;
    private byte[] _userImage;

    public Trailer() {}
    public Trailer(String urlTitle, String trailerName, byte[] trailerByte, String userName, byte[] userImage) {
        _urlTitle = urlTitle;
        _trailerName = trailerName;
        _trailerByte = trailerByte;
        _userName = userName;
        _userImage = userImage;
    }

    public String get_urlTitle() {
        return _urlTitle;
    }

    public void set_urlTitle(String _urlTitle) {
        this._urlTitle = _urlTitle;
    }

    public String get_trailerName() {
        return _trailerName;
    }

    public void set_trailerName(String _trailerName) {
        this._trailerName = _trailerName;
    }

    public byte[] get_trailerByte() {
        return _trailerByte;
    }

    public void set_trailerByte(byte[] _trailerByte) {
        this._trailerByte = _trailerByte;
    }

    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public byte[] get_userImage() {
        return _userImage;
    }

    public void set_userImage(byte[] _userImage) {
        this._userImage = _userImage;
    }
}
