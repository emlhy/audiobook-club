package mobile.jason.audiobookclub.entities;

/**
 * Created by Demon Pilot on 2015/3/17.
 */
public class AudioClip {
    private String _urlTitle;
    private int _chapterNumber;
    private String _audioName;
    private byte[] _audioByte;
    private String _userName;
    private byte[] _userImage;

    public AudioClip() {}
    public AudioClip(String urlTitle, int chapterNumber, String audioName, byte[] audioByte, String userName, byte[] userImage) {
        _urlTitle = urlTitle;
        _chapterNumber = chapterNumber;
        _audioName = audioName;
        _audioByte = audioByte;
        _userName = userName;
        _userImage = userImage;
    }

    public String get_urlTitle() {
        return _urlTitle;
    }

    public void set_urlTitle(String _urlTitle) {
        this._urlTitle = _urlTitle;
    }

    public int get_chapterNumber() {
        return _chapterNumber;
    }

    public void set_chapterNumber(int _chapterNumber) {
        this._chapterNumber = _chapterNumber;
    }

    public String get_audioName() {
        return _audioName;
    }

    public void set_audioName(String _audioName) {
        this._audioName = _audioName;
    }

    public byte[] get_audioByte() {
        return _audioByte;
    }

    public void set_audioByte(byte[] _audioByte) {
        this._audioByte = _audioByte;
    }

    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public byte[] get_userImage() {
        return _userImage;
    }

    public void set_userImage(byte[] _userImage) {
        this._userImage = _userImage;
    }
}
