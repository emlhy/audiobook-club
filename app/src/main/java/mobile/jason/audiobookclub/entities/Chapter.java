package mobile.jason.audiobookclub.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jason on 3/2/2015.
 */
public class Chapter implements Parcelable {
    private int _number;
    private String _name;
    private String _filePath;

    public int get_number() {
        return _number;
    }

    public void set_number(int _number) {
        this._number = _number;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_filePath() {
        return _filePath;
    }

    public void set_filePath(String _filePath) {
        this._filePath = _filePath;
    }

    public Chapter() {

    }

    protected Chapter(Parcel in) {
        _number = in.readInt();
        _name = in.readString();
        _filePath = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_number);
        dest.writeString(_name);
        dest.writeString(_filePath);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Chapter> CREATOR = new Parcelable.Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };
}
