package mobile.jason.audiobookclub.entities;

import com.ndu.mobile.daisy.providers.librivox.LVoxBook;
import com.ndu.mobile.daisy.providers.librivox.LVoxRSS;

/**
 * Created by Jason on 3/17/2015.
 */
public class LVoxBookWrapper {
    private LVoxBook mBook;
    private LVoxRSS mDetails;

    public LVoxBookWrapper(LVoxBook book, LVoxRSS details) {
        mBook = book;
        mDetails = details;
    }

    public String getId(){
        return mBook.getId();
    }

    public String getTitle() {
        return mBook.getTitle();
    }

    public String getThumbnailURL() {
        return mDetails.getThumbnailURL();
    }

    public String getCoverArtURL() {
        return mDetails.getCoverArtURL();
    }

    public String getAuthor() {
        return mBook.getAuthor();
    }

    public String getDescription() {
        return mBook.getDescription();
    }

    public int getChapterNumber() {
        return mDetails.getChapters().size();
    }
}
