package mobile.jason.audiobookclub.entities;

/**
 * Created by Jason on 3/31/2015.
 */
public class Badge {
    private String _badgeName;
    private String _encouragement;
    private String _description;
    private int _iconResId;

    public Badge (String badgeName, String encouragement, String description, int iconResId) {
        _badgeName = badgeName;
        _encouragement = encouragement;
        _description = description;
        _iconResId = iconResId;
    }

    public String get_badgeName() {
        return _badgeName;
    }

    public void set_badgeName(String _badgeName) {
        this._badgeName = _badgeName;
    }

    public String get_encouragement() {
        return _encouragement;
    }

    public void set_encouragement(String _encouragement) {
        this._encouragement = _encouragement;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public int get_iconResId() {
        return _iconResId;
    }

    public void set_iconResId(int _iconResId) {
        this._iconResId = _iconResId;
    }
}
