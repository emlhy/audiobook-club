package mobile.jason.audiobookclub.entities;

/**
 * Created by Demon Pilot on 2015/4/1.
 */
public class UserProfile {
    String _personName;
    byte[] _personImage;
    String _personGooglePlusProfile;
    String _email;

    public UserProfile() {}
    public UserProfile(String personName, byte[] personImage, String personGooglePlusProfile, String email) {
        _personName = personName;
        _personImage = personImage;
        _personGooglePlusProfile = personGooglePlusProfile;
        _email = email;
    }

    public String get_personName() {
        return _personName;
    }

    public void set_personName(String _personName) {
        this._personName = _personName;
    }

    public byte[] get_personImage() {
        return _personImage;
    }

    public void set_personImage(byte[] _personImage) {
        this._personImage = _personImage;
    }

    public String get_personGooglePlusProfile() {
        return _personGooglePlusProfile;
    }

    public void set_personGooglePlusProfile(String _personGooglePlusProfile) {
        this._personGooglePlusProfile = _personGooglePlusProfile;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }
}
