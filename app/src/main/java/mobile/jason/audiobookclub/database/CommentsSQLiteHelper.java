package mobile.jason.audiobookclub.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import mobile.jason.audiobookclub.entities.AudioClip;
import mobile.jason.audiobookclub.entities.Comment;
import mobile.jason.audiobookclub.entities.Trailer;
import mobile.jason.audiobookclub.entities.UserProfile;

/**
 * Created by Jason on 3/3/2015.
 */
public class CommentsSQLiteHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Comments.db";

    public static abstract class CommentsEntry implements BaseColumns {
        public static final String TABLE_NAME = "comments";
        public static final String COLUMN_NAME_BOOK_URL_TITLE = "bookUrlTitle";
        public static final String COLUMN_NAME_CHAPTER_NUMBER = "chapterNumber";
        public static final String COLUMN_NAME_COMMENT_BODY = "commentBody";
        public static final String COLUMN_NAME_USER_NAME = "userName";
        public static final String COLUMN_NAME_USER_IMAGE = "userImage";
    }

    public static abstract class AudioClipsEntry implements BaseColumns {
        public static final String TABLE_NAME = "audioClips";
        public static final String COLUMN_NAME_BOOK_URL_TITLE = "bookUrlTitle";
        public static final String COLUMN_NAME_CHAPTER_NUMBER = "chapterNumber";
        public static final String COLUMN_NAME_AUDIO_NAME = "audioName";
        public static final String COLUMN_NAME_AUDIO = "audio";
        public static final String COLUMN_NAME_USER_NAME = "userName";
        public static final String COLUMN_NAME_USER_IMAGE = "userImage";
    }

    public static abstract class TrailerEntry implements BaseColumns {
        public static final String TABLE_NAME = "trailer";
        public static final String COLUMN_NAME_BOOK_URL_TITLE = "bookUrlTitle";
        public static final String COLUMN_NAME_TRAILER_NAME = "trailerName";
        public static final String COLUMN_NAME_TRAILER = "trailer";
        public static final String COLUMN_NAME_USER_NAME = "userName";
        public static final String COLUMN_NAME_USER_IMAGE = "userImage";
    }

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_USER_NAME = "userName";
        public static final String COLUMN_NAME_USER_IMAGE = "userImage";
        public static final String COLUMN_NAME_USER_PROFILE = "userProfile";
        public static final String COLUMN_NAME_EMAIL = "email";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + CommentsEntry.TABLE_NAME + " (" +
                    CommentsEntry.COLUMN_NAME_BOOK_URL_TITLE + TEXT_TYPE + COMMA_SEP +
                    CommentsEntry.COLUMN_NAME_CHAPTER_NUMBER + INTEGER_TYPE + COMMA_SEP +
                    CommentsEntry.COLUMN_NAME_COMMENT_BODY + TEXT_TYPE + COMMA_SEP +
                    CommentsEntry.COLUMN_NAME_USER_NAME + TEXT_TYPE + COMMA_SEP +
                    CommentsEntry.COLUMN_NAME_USER_IMAGE + BLOB_TYPE +
                    " )";
    private static final String SQL_AUDIO_CLIPS =
            "CREATE TABLE " + AudioClipsEntry.TABLE_NAME + " (" +
                    AudioClipsEntry.COLUMN_NAME_BOOK_URL_TITLE + TEXT_TYPE + COMMA_SEP +
                    AudioClipsEntry.COLUMN_NAME_CHAPTER_NUMBER + INTEGER_TYPE + COMMA_SEP +
                    AudioClipsEntry.COLUMN_NAME_AUDIO_NAME + TEXT_TYPE + COMMA_SEP +
                    AudioClipsEntry.COLUMN_NAME_AUDIO + BLOB_TYPE + COMMA_SEP +
                    AudioClipsEntry.COLUMN_NAME_USER_NAME + TEXT_TYPE + COMMA_SEP +
                    AudioClipsEntry.COLUMN_NAME_USER_IMAGE + BLOB_TYPE +
                    " )";
    private static final String SQL_TRAILER =
            "CREATE TABLE " + TrailerEntry.TABLE_NAME + " (" +
                    TrailerEntry.COLUMN_NAME_BOOK_URL_TITLE + TEXT_TYPE + COMMA_SEP +
                    TrailerEntry.COLUMN_NAME_TRAILER_NAME + TEXT_TYPE + COMMA_SEP +
                    TrailerEntry.COLUMN_NAME_TRAILER + BLOB_TYPE + COMMA_SEP +
                    TrailerEntry.COLUMN_NAME_USER_NAME + TEXT_TYPE + COMMA_SEP +
                    TrailerEntry.COLUMN_NAME_USER_IMAGE + BLOB_TYPE +
                    " )";
    private static final String SQL_USER =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry.COLUMN_NAME_USER_NAME + TEXT_TYPE + COMMA_SEP +
                    UserEntry.COLUMN_NAME_USER_IMAGE + BLOB_TYPE + COMMA_SEP +
                    UserEntry.COLUMN_NAME_USER_PROFILE + TEXT_TYPE + COMMA_SEP +
                    UserEntry.COLUMN_NAME_EMAIL + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + CommentsEntry.TABLE_NAME;
    private static final String SQL_DELETE_AUDIO =
            "DROP TABLE IF EXISTS " + AudioClipsEntry.TABLE_NAME;
    private static final String SQL_DELETE_TRAILER =
            "DROP TABLE IF EXISTS " + TrailerEntry.TABLE_NAME;
    private static final String SQL_DELETE_USER =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;

    public CommentsSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_AUDIO_CLIPS);
        db.execSQL(SQL_TRAILER);
        db.execSQL(SQL_USER);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_DELETE_AUDIO);
        db.execSQL(SQL_DELETE_TRAILER);
        db.execSQL(SQL_DELETE_USER);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void dropTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
        db.execSQL(SQL_DELETE_AUDIO);
        db.execSQL(SQL_DELETE_TRAILER);
        db.execSQL(SQL_DELETE_USER);
        onCreate(db);
    }

    public void addComment(Comment comment){
        Log.d("addComment", "urlTitle = " + comment.get_urlTitle() + ", chapterNumber = " + comment.get_chapterNumber() + ", commentBody = " + comment.get_commentBody());
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(CommentsEntry.COLUMN_NAME_BOOK_URL_TITLE, comment.get_urlTitle());
        values.put(CommentsEntry.COLUMN_NAME_CHAPTER_NUMBER, comment.get_chapterNumber());
        values.put(CommentsEntry.COLUMN_NAME_COMMENT_BODY, comment.get_commentBody());
        values.put(CommentsEntry.COLUMN_NAME_USER_NAME, comment.get_userName());
        values.put(CommentsEntry.COLUMN_NAME_USER_IMAGE, comment.get_userImage());

        db.insert(CommentsEntry.TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public List<Comment> getComments(String urlTitle, int chapterNumber) {
        List<Comment> comments = new LinkedList<>();

        // 1. build the query
        String query = "SELECT  * FROM " + CommentsEntry.TABLE_NAME +
                " WHERE " + CommentsEntry.COLUMN_NAME_BOOK_URL_TITLE + " = \"" + urlTitle + "\"" +
                " AND " + CommentsEntry.COLUMN_NAME_CHAPTER_NUMBER + " = " + chapterNumber;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                comments.add(new Comment(cursor.getString(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getBlob(4)));
            } while (cursor.moveToNext());
        }

        Log.d("getComments()", comments.toString());

        return comments;
    }

    public void addAudioClip(AudioClip audioClip){
        Log.d("addAudioClip", "urlTitle = " + audioClip.get_urlTitle() + ", chapterNumber = " + audioClip.get_chapterNumber() + ", audioName = " + audioClip.get_audioName() + ", byteArray = " + audioClip.get_audioByte());
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(AudioClipsEntry.COLUMN_NAME_BOOK_URL_TITLE, audioClip.get_urlTitle());
        values.put(AudioClipsEntry.COLUMN_NAME_CHAPTER_NUMBER, audioClip.get_chapterNumber());
        values.put(AudioClipsEntry.COLUMN_NAME_AUDIO_NAME, audioClip.get_audioName());
        values.put(AudioClipsEntry.COLUMN_NAME_AUDIO, audioClip.get_audioByte());
        values.put(AudioClipsEntry.COLUMN_NAME_USER_NAME, audioClip.get_userName());
        values.put(AudioClipsEntry.COLUMN_NAME_USER_IMAGE, audioClip.get_userImage());

        db.insert(AudioClipsEntry.TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public List<AudioClip> getAudioClips(String urlTitle, int chapterNumber) {
        List<AudioClip> audioClips = new LinkedList<>();

        // 1. build the query
        String query = "SELECT  * FROM " + AudioClipsEntry.TABLE_NAME +
                " WHERE " + AudioClipsEntry.COLUMN_NAME_BOOK_URL_TITLE + " = \"" + urlTitle + "\"" +
                " AND " + AudioClipsEntry.COLUMN_NAME_CHAPTER_NUMBER + " = " + chapterNumber;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                audioClips.add(new AudioClip(cursor.getString(0), cursor.getInt(1), cursor.getString(2), cursor.getBlob(3), cursor.getString(4), cursor.getBlob(5)));
            } while (cursor.moveToNext());
        }

        Log.d("getComments()", audioClips.toString());

        return audioClips;
    }

    public void addTrailer(Trailer trailer){
        Log.d("addAudioClip", "urlTitle = " + trailer.get_urlTitle() + ", trailerName = " + trailer.get_trailerName() + ", byteArray = " + trailer.get_trailerByte());
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(TrailerEntry.COLUMN_NAME_BOOK_URL_TITLE, trailer.get_urlTitle());
        values.put(TrailerEntry.COLUMN_NAME_TRAILER_NAME, trailer.get_trailerName());
        values.put(TrailerEntry.COLUMN_NAME_TRAILER, trailer.get_trailerByte());
        values.put(AudioClipsEntry.COLUMN_NAME_USER_NAME, trailer.get_userName());
        values.put(AudioClipsEntry.COLUMN_NAME_USER_IMAGE, trailer.get_userImage());

        db.insert(TrailerEntry.TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public List<Trailer> getTrailer(String urlTitle) {
        List<Trailer> trailer = new LinkedList<>();

        // 1. build the query
        String query = "SELECT  * FROM " + TrailerEntry.TABLE_NAME +
                " WHERE " + TrailerEntry.COLUMN_NAME_BOOK_URL_TITLE + " = \"" + urlTitle + "\"";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                trailer.add(new Trailer(cursor.getString(0), cursor.getString(1), cursor.getBlob(2), cursor.getString(3), cursor.getBlob(4)));
            } while (cursor.moveToNext());
        }

        Log.d("getComments()", trailer.toString());

        return trailer;
    }

    public void addUser(UserProfile userProfile){

        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_USER_NAME, userProfile.get_personName());
        values.put(UserEntry.COLUMN_NAME_USER_IMAGE, userProfile.get_personImage());
        values.put(UserEntry.COLUMN_NAME_USER_PROFILE, userProfile.get_personGooglePlusProfile());
        values.put(UserEntry.COLUMN_NAME_EMAIL, userProfile.get_email());

        db.insert(UserEntry.TABLE_NAME, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        db.close();
    }

    public UserProfile getUser(String email) {
        UserProfile user = new UserProfile();

        // 1. build the query
        String query = "SELECT  * FROM " + UserEntry.TABLE_NAME +
                " WHERE " + UserEntry.COLUMN_NAME_EMAIL + " = \"" + email + "\"";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                user = new UserProfile(cursor.getString(0), cursor.getBlob(1), cursor.getString(2), cursor.getString(3));
            } while (cursor.moveToNext());
        }

        Log.d("getComments()", user.toString());

        return user;
    }
}
