package mobile.jason.audiobookclub.managers;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Ding on 3/23/2015.
 */
public class MediaManager {

    public enum States{
        Uninitialized,
        Initialized,
        Prepared,
        Started,
        Paused,
        Stopped
    }

    private static MediaManager mediaManager;
    private MediaPlayer mediaPlayer;
    private States state;


    private MediaManager(){
        this.state = States.Uninitialized;
    }

    public synchronized static MediaManager getInstance(){
        if(mediaManager == null){
            mediaManager = new MediaManager();
        }

        return mediaManager;
    }

   public MediaPlayer getMediaPlayer(){
       prepareMediaPlayer();
       return this.mediaPlayer;
   }

    public void prepareMediaPlayer(){
        if(this.mediaPlayer == null) {
            this.mediaPlayer = new MediaPlayer();
            this.state = States.Initialized;
        }
    }

    public boolean isPlaying(){
        return this.mediaPlayer != null && this.mediaPlayer.isPlaying();
    }

    public void start(){
        if(this.mediaPlayer != null && !this.isPlaying()){
            this.mediaPlayer.start();
            this.state = States.Started;
        }
    }

    public void stop(){
        if(this.mediaPlayer != null){
            this.mediaPlayer.stop();
            this.state = States.Stopped;
            this.mediaPlayer.release();
            this.mediaPlayer = null;
            this.state = States.Uninitialized;
        }
    }

    public void pause(){
        if(this.mediaPlayer != null && this.isPlaying()){
            this.mediaPlayer.pause();
            this.state = States.Paused;
        }
    }

    public void setDataSource(String path) throws IOException{
        if(this.mediaPlayer != null) {
            this.mediaPlayer.setDataSource(path);
            this.mediaPlayer.prepare();
            this.state = States.Prepared;
        }
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener){
        if(this.mediaPlayer != null){
            this.mediaPlayer.setOnCompletionListener(listener);
        }
    }

    public int getDuration(){
        if(this.mediaPlayer != null){
            return this.mediaPlayer.getDuration();
        }
        return -1;
    }

    public int getCurrentPosition(){
        if(this.mediaPlayer != null){
            return this.mediaPlayer.getCurrentPosition();
        }

       return -1;
    }

    public void seekTo(int pos){
        if(this.mediaPlayer != null){
            this.mediaPlayer.seekTo(pos);
        }
    }

    public States getState(){
        return this.state;
    }
}
