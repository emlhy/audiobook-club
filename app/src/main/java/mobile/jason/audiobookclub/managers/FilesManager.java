package mobile.jason.audiobookclub.managers;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Environment;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.entities.Chapter;

/**
 * Created by Jason on 3/2/2015.
 */
public class FilesManager {
    private static FilesManager mFilesManager;
    private boolean mLoaded = false;
    private List<Audiobook> mBooks;

    private FilesManager() {
        mBooks = new ArrayList<Audiobook>();
    }

    public static FilesManager getInstance() {
        if (mFilesManager == null) {
            mFilesManager = new FilesManager();
        }
        return mFilesManager;
    }

    public List<Audiobook> getMyBooks(Context context) {
        if (!mLoaded) {
            loadMyBooks(context);
            mLoaded = true;
        }
        return mBooks;
    }

    public Audiobook getMyAudiobook(String urlTitle) {
        Iterator<Audiobook> iter = mBooks.iterator();

        while (iter.hasNext()) {
            Audiobook book = iter.next();
            if (book.get_urlTitle().equalsIgnoreCase(urlTitle)) {
                return book;
            }
        }

        return null;
    }

    public void registerNewBook(Context context, String urlTitle) {
        File filesDir = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.my_books_files_dir));
        try {
            filesDir.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File folder = new File(filesDir, urlTitle);

        if (folder != null) {
            loadFromFolders(new File[]{folder});
        }
    }

    public int getChapterDurationSeconds(String urlTitle, int chapterNumber) {
        Chapter chapter = getMyAudiobook(urlTitle).get_chapters().get(chapterNumber);
        File file = new File(chapter.get_filePath());
        MediaPlayer mp = new MediaPlayer();
        FileInputStream fs;
        FileDescriptor fd;
        int length = 0;
        try {
            fs = new FileInputStream(file);
            fd = fs.getFD();
            mp.setDataSource(fd);
            mp.prepare();
            length = mp.getDuration();
            mp.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length/1000;
    }

    private void loadMyBooks(Context context) {
        File filesDir = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.my_books_files_dir));
        try {
            filesDir.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File[] librivoxFolder = filesDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().contains("librivox");
            }
        });

        if (librivoxFolder != null) {
            loadFromFolders(librivoxFolder);
        }
    }

    private void loadFromFolders(File[] folders) {

        for (File book : folders) {
            Audiobook audiobook = new Audiobook();

            File[] audioChapters = book.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".mp3");
                }
            });

            File thumbnail = null;
            File coverArt = null;
            for (File file : book.listFiles()) {
                String fileName = file.getName().toLowerCase();
                if (fileName.endsWith("thumb.jpg")) {
                    thumbnail = file;
                } else if (fileName.endsWith(".jpg")) {
                    coverArt = file;
                }
            }

            File[] meta = book.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.toLowerCase().endsWith(DownloadManager.METADATA_POSTFILE));
                }
            });

            audiobook.set_urlTitle(book.getName());
            if (thumbnail != null) {
                audiobook.set_thumbnail(BitmapFactory.decodeFile(thumbnail.getPath()));
            }
            if (coverArt != null) {
                audiobook.set_coverArt(BitmapFactory.decodeFile(coverArt.getPath()));
            }

            JSONObject obj = readJSONObjectFromFile(meta[0]);
            audiobook.set_description((String) obj.get(DownloadManager.JSON_DESCRIPTION));
            Long timeLong = (Long) obj.get(DownloadManager.JSON_TOTAL_LENGTH);
            audiobook.set_totalTimeSecs(timeLong.intValue());
            //audiobook.set_description(readJSONObjectFromFile(meta[0]));

            for (int i = 0; i < audioChapters.length; i++) {
                File chap = audioChapters[i];
                Mp3File mp3File = null;
                Chapter chapter = new Chapter();
                try {
                    mp3File = new Mp3File(chap.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UnsupportedTagException e) {
                    e.printStackTrace();
                } catch (InvalidDataException e) {
                    e.printStackTrace();
                }

                if (mp3File != null) {
                    chapter.set_filePath(chap.getPath());
                    if (mp3File.hasId3v1Tag()) {
                        ID3v1 tag = mp3File.getId3v1Tag();
                        chapter.set_name(tag.getTitle());
                        if (tag.getTrack() != null && !tag.getTrack().isEmpty()) {
                            chapter.set_number(Integer.parseInt(tag.getTrack()));
                        } else {
                            chapter.set_number(0);
                        }

                        audiobook.add_author(tag.getArtist());
                        audiobook.set_title(tag.getAlbum());
                        audiobook.add_genre(tag.getGenreDescription());
                    }
                }
                audiobook.add_chapter(chapter);
            }

            mBooks.add(audiobook);
        }
    }

    private JSONObject readJSONObjectFromFile(File file) {
        FileInputStream fin = null;
        String ret = null;

        try {
            fin = new FileInputStream(file);
            ret = convertStreamToString(fin);
            fin.close();

            if (ret != null) {
                Object obj = new JSONParser().parse(new FileReader(file));
                return (JSONObject) obj;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }
}
