package mobile.jason.audiobookclub.managers;

/**
 * Created by Jason on 3/25/2015.
 */
public class WishlistManager {
    private static WishlistManager mWishlistManager;

    private WishlistManager(){
    }

    public static WishlistManager getInstance() {
        if (mWishlistManager == null) {
            mWishlistManager = new WishlistManager();
        }
        return mWishlistManager;
    }
}
