package mobile.jason.audiobookclub.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Audiobook;

/**
 * Created by Jason on 3/25/2015.
 */
public class ProgressManager {
    private static ProgressManager mProgressManager;
    private static final String PREF_LAST_BOOK_READ = "PREF_LAST_BOOK_READ";
    private static final String PREF_DURATION_REMAINING = "PREF_DURATION_REMAINING";
    private static final String PREF_TOTAL_LISTENING_TIME = "PREF_TOTAL_LISTENING_TIME";
    private static final String PREF_COMMENTS_MADE = "PREF_COMMENTS_MADE";
    private static final String PREF_BOOKS_DOWNLOADED = "PREF_BOOKS_DOWNLOADED";
    private static final String PREF_TRAILERS_RECORDED = "PREF_TRAILERS_RECORDED";

    private static final int NOVICE_LEVEL_REQUIREMENT_SECONDS = 86400;
    private static final int SCHOLAR_LEVEL_REQUIREMENT_SECONDS = 17280000;
    private static final int AUDIOPHILE_COMMENTS = 1;
    private static final int COLLECTOR_DOWNLOADS = 1;

    public enum Level {
        Newbie, Novice, Scholar;
        private static Level[] vals = values();
        public Level next() {
            return vals[(this.ordinal()+1) % vals.length];
        }
    };

    private ProgressManager() {

    }

    public static ProgressManager getInstance() {
        if (mProgressManager == null) {
            mProgressManager = new ProgressManager();
        }
        return mProgressManager;
    }

    public boolean isCommentUnlocked(Context context, String urlTitle, int chapterNumber) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int chapterCompleted = pref.getInt(urlTitle, -1);
        return chapterNumber <= chapterCompleted;
    }

    public int getLastChapterCompleted(Context context, String urlTitle) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(urlTitle, -1);
    }

    public boolean isBookCompleted(Context context, Audiobook book) {
        int lastChapterCompleted = getLastChapterCompleted(context, book.get_urlTitle());
        return (lastChapterCompleted >= book.get_chapters().size() - 1);
    }

    public void setCurrentChapterCompleted(Context context, String urlTitle, int chapterNumber) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putInt(urlTitle, chapterNumber).commit();

        int chapterDurationSeconds = FilesManager.getInstance().getChapterDurationSeconds(urlTitle, chapterNumber);
        int durationRemaining = pref.getInt(urlTitle + PREF_DURATION_REMAINING, FilesManager.getInstance().getMyAudiobook(urlTitle).get_totalTimeSecs());
        pref.edit().putInt(urlTitle + PREF_DURATION_REMAINING, durationRemaining - chapterDurationSeconds).commit();
        addToTotalTime(context, chapterDurationSeconds);
    }

    public int getBookDurationRemaining(Context context, String urlTitle) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(urlTitle + PREF_DURATION_REMAINING, FilesManager.getInstance().getMyAudiobook(urlTitle).get_totalTimeSecs());
    }

    public String getLastBookListened(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString(PREF_LAST_BOOK_READ, "");
    }

    public void setLastBookListened(Context context, String urlTitle) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString(PREF_LAST_BOOK_READ, urlTitle).commit();
    }

    public int getTotalTime(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getInt(PREF_TOTAL_LISTENING_TIME, 0);
    }

    public String getTotalListeningTimeText(Context context) {
        int totalListeningTimeSec = getTotalTime(context);
        int TotalMin = totalListeningTimeSec/60;
        int TotalHr = TotalMin/60;
        int day = TotalHr/24;
        int hr = TotalHr%60;
        int min = TotalMin%60;
        NumberFormat f = new DecimalFormat("00");
        String str = context.getResources().getString(R.string.format_total_listening_time);

        return String.format(str, f.format(day), f.format(hr), f.format(min));
    }

    public void addToTotalTime(Context context, int seconds) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putInt(PREF_TOTAL_LISTENING_TIME, getTotalTime(context) + seconds).commit();
    }

    public Level getLevel(Context context) {
        int seconds = getTotalTime(context);

        if (seconds >= SCHOLAR_LEVEL_REQUIREMENT_SECONDS) {
            return Level.Scholar;
        }
        if (seconds >= NOVICE_LEVEL_REQUIREMENT_SECONDS) {
            return Level.Novice;
        }
        return Level.Newbie;
    }

    public int getNextLevelTime(Context context) {
        int currentTotalTime = ProgressManager.getInstance().getTotalTime(context);
        int time = NOVICE_LEVEL_REQUIREMENT_SECONDS;
        switch(getLevel(context)) {
            case Scholar:
                time =  -1;
            case Novice:
                time = SCHOLAR_LEVEL_REQUIREMENT_SECONDS - currentTotalTime;
            case Newbie:
                time = NOVICE_LEVEL_REQUIREMENT_SECONDS - currentTotalTime;
        }

        return time;
    }

    public String getNextLevelupText(Context context, ProgressManager.Level level) {
        Resources res = context.getResources();
        int timeToLevelup = ProgressManager.getInstance().getNextLevelTime(context);
        if (timeToLevelup < 0) {
            return res.getString(R.string.highest_level);
        }

        int TotalMin = timeToLevelup/60;
        int hr = TotalMin/60;
        int min = TotalMin%60;
        String str = context.getResources().getString(R.string.format_next_levelup);

        return String.format(str, hr, min, level.next());
    }

    public boolean badgeEnabledAudiophile(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int commentsMade = pref.getInt(PREF_COMMENTS_MADE, 0);
        return commentsMade >= AUDIOPHILE_COMMENTS;
    }

    public void addAudioCommentMade(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int commentsMade = pref.getInt(PREF_COMMENTS_MADE, 0);
        pref.edit().putInt(PREF_COMMENTS_MADE, commentsMade+1).commit();
        if (commentsMade + 1 == AUDIOPHILE_COMMENTS) {
            BadgeManager.getInstance().showBadgeAcquiredDialog(context, BadgeManager.BADGE_AUDIOPHILE);
        }
    }

    public boolean badgeEnabledCollector(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int commentsMade = pref.getInt(PREF_BOOKS_DOWNLOADED, 0);
        return commentsMade >= COLLECTOR_DOWNLOADS;
    }

    public void addDownloadComplete(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int downloaded = pref.getInt(PREF_BOOKS_DOWNLOADED, 0);
        pref.edit().putInt(PREF_BOOKS_DOWNLOADED, downloaded+1).commit();
        if (downloaded + 1 == COLLECTOR_DOWNLOADS) {
            BadgeManager.getInstance().showBadgeAcquiredDialog(context, BadgeManager.BADGE_COLLECTOR);
        }
    }

    public boolean badgeEnabledProducer(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return (pref.getInt(PREF_TRAILERS_RECORDED, 0) > 0);
    }

    public void addRecordedTrailer(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int trailersRecorded = pref.getInt(PREF_TRAILERS_RECORDED, 0);
        pref.edit().putInt(PREF_TRAILERS_RECORDED, trailersRecorded + 1).commit();
        if (trailersRecorded == 0) {
            BadgeManager.getInstance().showBadgeAcquiredDialog(context, BadgeManager.BADGE_PRODUCER);
        }
    }
}
