package mobile.jason.audiobookclub.managers;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mobile.jason.audiobookclub.Fragments.BadgeDialogFragment;
import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Badge;

/**
 * Created by Jason on 3/31/2015.
 */
public class BadgeManager {
    private static BadgeManager mBadgeManager;
    private List<Badge> mBadges;

    public static final String BADGE_AUDIOPHILE = "Audiophile";
    public static final String BADGE_CAPTAIN = "Captain";
    public static final String BADGE_COLLECTOR = "Collector";
    public static final String BADGE_COMMENTATOR = "Commentator";
    public static final String BADGE_PRODUCER = "Producer";
    public static final String BADGE_DOOR_OPENER = "Door Opener";
    public static final String BADGE_UNLOCKER = "Unlocker";
    public static final String BADGE_ADJUSTER = "Adjuster";
    public static final String BADGE_ARTIST = "Artist";
    public static final String BADGE_BRIGHT_IDEA = "Bright Idea";
    public static final String BADGE_CONDUCTOR = "Conductor";
    public static final String BADGE_CREDIT_CARD = "Credit Card";
    public static final String BADGE_DOWNLOAD_CLOUD = "Download Cloud";
    public static final String BADGE_DRIVER = "Driver";
    public static final String BADGE_GARDENER = "Gardener";
    public static final String BADGE_HIGH_SCORER = "High Scorer";
    public static final String BADGE_LISTENER = "Listener";
    public static final String BADGE_LOVER = "Lover";
    public static final String BADGE_NIGHT_OWL = "Night Owl";
    public static final String BADGE_RECYCLER = "Recycler";
    public static final String BADGE_SAVER = "Saver";
    public static final String BADGE_SUPER_STAR = "Super Star";
    public static final String BADGE_WISH_MASTER = "Wish Master";
    public static final String BADGE_WORLD_READER = "World Reader";
    public static final String BADGE_WRITER = "Writer";



    private BadgeManager () {
        mBadges = new ArrayList<>();
        mBadges.add(new Badge(BADGE_AUDIOPHILE, "You’re a true hi-fi enthusiast!", "You’ve created 20 audio comments", R.drawable.recorder3));
        mBadges.add(new Badge(BADGE_CAPTAIN, "Great job steering the conversation!", "You’ve replied to 5 comments", R.drawable.captain));
        mBadges.add(new Badge(BADGE_COLLECTOR, "Wow you’re library is full!", "You’ve downloaded 15 audiobooks", R.drawable.collector));
        mBadges.add(new Badge(BADGE_COMMENTATOR, "Great job entertaining the audiobook club with your comments!", "You’ve written 15 comments", R.drawable.commentator));
        mBadges.add(new Badge(BADGE_PRODUCER, "What a great ear for production you have!", "You’ve created your first book trailer", R.drawable.producer2));
        mBadges.add(new Badge(BADGE_DOOR_OPENER, "Thank you for opening the conversation!", "You were the first the comment on a chapter!", R.drawable.door_opener));
        mBadges.add(new Badge(BADGE_UNLOCKER, "Those discussions don’t stand a chance against you!", "You’ve unlocked discussions for 25 chapters", R.drawable.unlockers));
        mBadges.add(new Badge(BADGE_ADJUSTER, "", "", R.drawable.adjuster));
        mBadges.add(new Badge(BADGE_ARTIST, "", "", R.drawable.artist));
        mBadges.add(new Badge(BADGE_BRIGHT_IDEA, "", "", R.drawable.bright_idea));
        mBadges.add(new Badge(BADGE_CONDUCTOR, "", "", R.drawable.conductor));
        mBadges.add(new Badge(BADGE_CREDIT_CARD, "", "", R.drawable.credit_card));
        mBadges.add(new Badge(BADGE_DOWNLOAD_CLOUD, "", "", R.drawable.download_cloud));
        mBadges.add(new Badge(BADGE_DRIVER, "", "", R.drawable.driver));
        mBadges.add(new Badge(BADGE_GARDENER, "", "", R.drawable.gardener));
        mBadges.add(new Badge(BADGE_HIGH_SCORER, "", "", R.drawable.high_scorer));
        mBadges.add(new Badge(BADGE_LISTENER, "", "", R.drawable.listener));
        mBadges.add(new Badge(BADGE_LOVER, "", "", R.drawable.lover));
        mBadges.add(new Badge(BADGE_NIGHT_OWL, "", "", R.drawable.night_owl));
        mBadges.add(new Badge(BADGE_RECYCLER, "", "", R.drawable.recycler));
        mBadges.add(new Badge(BADGE_SAVER, "", "", R.drawable.saver));
        mBadges.add(new Badge(BADGE_SUPER_STAR, "", "", R.drawable.superstar));
        mBadges.add(new Badge(BADGE_WISH_MASTER, "", "", R.drawable.wish_master));
        mBadges.add(new Badge(BADGE_WORLD_READER, "", "", R.drawable.world_reader));
        mBadges.add(new Badge(BADGE_WRITER, "", "", R.drawable.writer));
    }

    public static BadgeManager getInstance() {
        if (mBadgeManager == null) {
            mBadgeManager = new BadgeManager();
        }
        return mBadgeManager;
    }

    public List<Badge> getBadges() {
        return mBadges;
    }

    public boolean isBadgeUnlocked(Context context, Badge badge) {
        ProgressManager progressManager = ProgressManager.getInstance();
        String badgeTitle = badge.get_badgeName();

        if (badgeTitle.equalsIgnoreCase(BADGE_AUDIOPHILE)) {
            return progressManager.badgeEnabledAudiophile(context);
        }
        if (badgeTitle.equalsIgnoreCase(BADGE_COLLECTOR)) {
            return progressManager.badgeEnabledCollector(context);
        }
        if (badgeTitle.equalsIgnoreCase(BADGE_PRODUCER)) {
            return progressManager.badgeEnabledProducer(context);
        }
        //set always enabled for the demo
        if (badgeTitle.equalsIgnoreCase(BADGE_DOOR_OPENER)) {
            return true;
        }
        if (badgeTitle.equalsIgnoreCase(BADGE_UNLOCKER)) {
            return true;
        }

        return false;
    }

    public void showBadgeAcquiredDialog(Context context, String badgeTitle) {
        Badge acquiredBadge = null;
        for (Badge badge : mBadges) {
            if (badge.get_badgeName().equalsIgnoreCase(badgeTitle)) {
                acquiredBadge = badge;
                break;
            }
        }

        if (acquiredBadge != null) {
            BadgeDialogFragment diag = new BadgeDialogFragment(acquiredBadge);
            try {
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                diag.show(fragmentManager, "badge");
            } catch (ClassCastException e) {
                Log.e("BadgeManager", "Can't get fragment manager");
            }
        }
    }
}
