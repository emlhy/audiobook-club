package mobile.jason.audiobookclub.managers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.ndu.mobile.daisy.providers.exceptions.NDULibraryConnectionException;
import com.ndu.mobile.daisy.providers.exceptions.NDULibraryParsingException;
import com.ndu.mobile.daisy.providers.librivox.LVoxBook;
import com.ndu.mobile.daisy.providers.librivox.LVoxChapter;
import com.ndu.mobile.daisy.providers.librivox.LVoxRSS;
import com.ndu.mobile.daisy.providers.librivox.LVoxRestClient;

import org.json.simple.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.LVoxBookWrapper;

/**
 * Created by Jason on 3/2/2015.
 */
public class DownloadManager {
    private static DownloadManager mDownloadManager;
    LVoxRestClient mClient;
    private boolean mLoadedLatest;
    private List<LVoxBookWrapper> mLatestBooks;

    private static final String DOWNLOAD_PREURL = "http://www.archive.org/download/";
    private static final String ZIP_DOWNLOAD_POSTURL = "_64kb_mp3.zip";
    public static final String METADATA_POSTFILE = "_metadata.txt";
    private static final int BUFFER_SIZE = 8192;
    public static final String JSON_DESCRIPTION = "JSON_DESCRIPTION";
    public static final String JSON_TOTAL_LENGTH = "JSON_TOTAL_LENGTH";

    private DownloadManager() {
        mLatestBooks = new ArrayList<>();
        mClient = new LVoxRestClient();
        mLoadedLatest = false;
    }

    public static DownloadManager getInstance() {
        if (mDownloadManager == null) {
            mDownloadManager = new DownloadManager();
        }
        return mDownloadManager;
    }

    public void loadLatestBooks() {
        new LoadLatestTask().execute();
    }

    public List<LVoxBookWrapper> getLatestBooks() {
        return mLatestBooks;
    }

    public List<LVoxBookWrapper> searchTitlesAndAuthors(String searchString) {
        List<LVoxBook> searchResult = new ArrayList<>();
        List<LVoxBookWrapper> wrapperResult = new ArrayList<>();
        try {
            searchResult.addAll(mClient.searchByTitle(searchString));
            searchResult.addAll(mClient.searchByAuthor(searchString));

            for (LVoxBook book : searchResult) {
                wrapperResult.add(new LVoxBookWrapper(book, getBookDetails(book.getId())));
            }
        } catch (NDULibraryConnectionException e) {
            e.printStackTrace();
        } catch (NDULibraryParsingException e) {
            e.printStackTrace();
        }

        return wrapperResult;
    }

    public void downloadAudioBook(Context context, String urlTitle, String description) throws Exception{
        LVoxRSS details = mClient.getBookDetails(urlTitle);
        String zipString = urlTitle + ZIP_DOWNLOAD_POSTURL;
        URLConnection zipCon = mClient.getDownloadChapterConnection(DOWNLOAD_PREURL + urlTitle + "/" + zipString);
        File folder = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.my_books_files_dir));
        try {
            folder.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File zipFile = new File(folder, zipString);
        downloadFile(zipCon, zipFile);

        File targetFolder = new File(folder, urlTitle);
        unzip(zipFile.getPath(), targetFolder.getPath());
        zipFile.delete();

        URLConnection thumbCon = mClient.getDownloadChapterConnection(details.getThumbnailURL());
        File thumbnailFile = new File(targetFolder, details.getThumbnailName());
        downloadFile(thumbCon, thumbnailFile);

        URLConnection coverArtCon = mClient.getDownloadChapterConnection(details.getCoverArtURL());
        File coverArtFile = new File(targetFolder, details.getCoverArtName());
        downloadFile(coverArtCon, coverArtFile);

        int totalPlaytime = 0;
        for (LVoxChapter chapter : details.getChapters()) {
            totalPlaytime += (int) chapter.getLengthSeconds();
        }
        File metadataFile = new File(targetFolder, urlTitle + METADATA_POSTFILE);
        writeMetadataToFile(metadataFile, description, totalPlaytime);

        FilesManager.getInstance().registerNewBook(context, urlTitle);
    }

    private void downloadFile(URLConnection connection, File fileDestination) throws Exception{
        FileOutputStream fileOutput = null;
        fileOutput = new FileOutputStream(fileDestination);
        InputStream inputStream = connection.getInputStream();

        byte[] buffer = new byte[1024];
        int bufferLength = 0;

        while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
            fileOutput.write(buffer, 0, bufferLength);
        }
        fileOutput.close();
    }

    private void writeMetadataToFile(File file, String description, int totalPlaytimeSeconds) throws Exception{
        JSONObject object = new JSONObject();
        object.put(JSON_DESCRIPTION, description);
        object.put(JSON_TOTAL_LENGTH, new Integer(totalPlaytimeSeconds));
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
        outputStreamWriter.write(object.toJSONString());
        outputStreamWriter.close();
    }

    private LVoxRSS getBookDetails(String bookId) {
        try {
            return mClient.getBookDetails(bookId);
        } catch (NDULibraryConnectionException e) {
            e.printStackTrace();
        } catch (NDULibraryParsingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class LoadLatestTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (mLatestBooks != null) {
                mLatestBooks.clear();
                List<LVoxBook> lvBooks = null;
                try {
                    lvBooks = mClient.getLatest(0);
                    for (LVoxBook lvBook : lvBooks) {
                        //avoid the empty format books without doing getBookDetails
                        if (!lvBook.getTitle().toLowerCase().contains("librivox") && !lvBook.getAuthor().toLowerCase().contains("librivox")) {
                            mLatestBooks.add(new LVoxBookWrapper(lvBook, getBookDetails(lvBook.getId())));
                        }
                    }
                    Log.d("loadLatestTask", "mLatestBooks = " + mLatestBooks.toString());
                } catch (NDULibraryConnectionException e) {
                    e.printStackTrace();
                } catch (NDULibraryParsingException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public void unzip(String zipFile, String location) throws IOException {
        int size;
        byte[] buffer = new byte[BUFFER_SIZE];

        try {
            if ( !location.endsWith("/") ) {
                location += "/";
            }
            File f = new File(location);
            if(!f.isDirectory()) {
                f.mkdirs();
            }
            ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
            try {
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    String path = location + ze.getName();
                    File unzipFile = new File(path);

                    if (ze.isDirectory()) {
                        if(!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        // check for and create parent directories if they don't exist
                        File parentDir = unzipFile.getParentFile();
                        if ( null != parentDir ) {
                            if ( !parentDir.isDirectory() ) {
                                parentDir.mkdirs();
                            }
                        }

                        /* unzip the file */
                        FileOutputStream out = new FileOutputStream(unzipFile, false);
                        BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
                        try {
                            while ( (size = zin.read(buffer, 0, BUFFER_SIZE)) != -1 ) {
                                fout.write(buffer, 0, size);
                            }

                            zin.closeEntry();
                        }
                        finally {
                            fout.flush();
                            fout.close();
                        }
                    }
                }
            }
            finally {
                zin.close();
            }
        }
        catch (Exception e) {
            Log.e("FilesManager", "Unzip exception", e);
        }
    }

    public void runDownloadAudiobookTask(Context context, String urlTitle, String description) {
        new DownloadAudiobookTask(context).execute(urlTitle, description);
    }

    private class DownloadAudiobookTask extends AsyncTask<String, Void, Boolean> {

        private ProgressDialog mDialog;
        private Context mContext;
        public DownloadAudiobookTask(Context context) {
            mDialog = new ProgressDialog(context);
            mDialog.setCanceledOnTouchOutside(false);
            mContext = context;
        }

        protected void onPreExecute() {
            mDialog.setMessage("Downloading Audiobook...");
            mDialog.show();
        }


        protected Boolean doInBackground(String... args) {
            try{
                downloadAudioBook(mContext, args[0], args[1]);
                return true;
            } catch (Exception e) {
                Log.e("DownloadAudiobookTask", "error", e);
            }
            return false;
        }


        @Override
        protected void onPostExecute(final Boolean success) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }

            if (success) {
                Toast.makeText(mContext, "Download Complete", Toast.LENGTH_SHORT).show();
                ProgressManager.getInstance().addDownloadComplete(mContext);
            } else {
                Toast.makeText(mContext, "Download Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
