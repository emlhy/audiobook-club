package mobile.jason.audiobookclub.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.entities.Badge;
import mobile.jason.audiobookclub.managers.BadgeManager;
import mobile.jason.audiobookclub.managers.FilesManager;

/**
 * Created by Jason on 3/3/2015.
 */
public final class BadgesGridAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private Context mContext;
    private List<Badge> mBadges;

    public BadgesGridAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mBadges = BadgeManager.getInstance().getBadges();
    }

    @Override
    public int getCount() {
        return mBadges.size();
    }

    @Override
    public Badge getItem(int i) {
        return mBadges.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.badge_grid_item, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.ivBadgeIcon = (ImageView) convertView.findViewById(R.id.badge_icon);
            viewHolder.tvBadgeTitle = (TextView) convertView.findViewById(R.id.badge_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Badge badge = getItem(i);

        if (BadgeManager.getInstance().isBadgeUnlocked(mContext, badge)) {
            viewHolder.ivBadgeIcon.setImageResource(badge.get_iconResId());
        } else {
            Drawable icon = mContext.getResources().getDrawable(badge.get_iconResId());
            viewHolder.ivBadgeIcon.setImageDrawable(convertToGrayscale(icon));
        }

        viewHolder.tvBadgeTitle.setText(badge.get_badgeName());

        return convertView;
    }

    private static class ViewHolder {
        ImageView ivBadgeIcon;
        TextView tvBadgeTitle;
    }

    private Drawable convertToGrayscale(Drawable drawable)
    {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

        drawable.setColorFilter(filter);

        return drawable;
    }
}
