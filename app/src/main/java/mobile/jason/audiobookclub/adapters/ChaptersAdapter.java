package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Chapter;
import mobile.jason.audiobookclub.managers.ProgressManager;

/**
 * Created by Jason on 3/3/2015.
 */
public class ChaptersAdapter extends ArrayAdapter<Chapter> {

    private List<Chapter> mChapters;
    private boolean mFromLocal = false;
    private int mFinishedChapter;
    private Context mContext;

    public ChaptersAdapter(Context context, List<Chapter> chapters, int finishedChapter) {
        super(context, R.layout.chapter_list_item, chapters);
        mChapters = chapters;
        mContext = context;
        mFromLocal = true;
        mFinishedChapter = finishedChapter;
    }

    public ChaptersAdapter(Context context, int numChapters) {
        super(context, R.layout.chapter_list_item, new ArrayList<Chapter>());
        List<Chapter> chapters = new ArrayList<>();

        for (int i = 0; i < numChapters; i++) {
            Chapter chapter = new Chapter();
            chapter.set_number(i);
            chapter.set_name("Chapter " + i);
            chapters.add(chapter);
        }

        mContext = context;
        mChapters = chapters;
        mFromLocal = false;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mChapters.size();
    }

    @Override
    public Chapter getItem(int position) {
        return mChapters.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.chapter_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.tvChapterTitle = (TextView) convertView.findViewById(R.id.chapter_title_text);
            viewHolder.ivChapterIcon = (ImageView) convertView.findViewById(R.id.chapter_icon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Chapter chapter = getItem(position);
        viewHolder.tvChapterTitle.setText(chapter.get_name());

        if (mFromLocal) {
            if (mFinishedChapter >= position) {
                viewHolder.ivChapterIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_accept));
            } else {
                viewHolder.ivChapterIcon.setImageDrawable(null);
            }
        } else {
            viewHolder.ivChapterIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_download));
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView tvChapterTitle;
        ImageView ivChapterIcon;
    }
}
