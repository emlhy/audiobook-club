package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Comment;

/**
 * Created by Jason on 3/3/2015.
 */
public class CommentsAdapter extends ArrayAdapter<Comment> {

    private List<Comment> mComments;
    public CommentsAdapter(Context context) {
        super(context, R.layout.chapter_list_item);
    }

    @Override
    public int getCount() {
        return mComments.size();
    }

    @Override
    public Comment getItem(int position) {
        return mComments.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.comment_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.getTvCommentBody = (TextView) convertView.findViewById(R.id.comment_body);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.userImage = (ImageView) convertView.findViewById(R.id.user_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Comment comment = getItem(position);
        viewHolder.getTvCommentBody.setText(comment.get_commentBody());
        viewHolder.userName.setText(comment.get_userName());
        byte[] byteArray = comment.get_userImage();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        viewHolder.userImage.setImageBitmap(bmp);

        return convertView;
    }

    public void setComments(List<Comment> comments) {
        mComments = comments;
    }

    private static class ViewHolder {
        TextView getTvCommentBody;
        TextView userName;
        ImageView userImage;
    }
}
