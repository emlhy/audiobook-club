package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.activities.MainActivity;
import mobile.jason.audiobookclub.entities.LVoxBookWrapper;
import mobile.jason.audiobookclub.managers.DownloadManager;

/**
 * Created by Jason on 3/3/2015.
 */
public final class BrowseBooksGridAdapter extends BaseAdapter {
    private List<LVoxBookWrapper> mBooks;
    private final LayoutInflater mInflater;
    private Context mContext;

    public BrowseBooksGridAdapter(Context context, List<LVoxBookWrapper> books) {
        mInflater = LayoutInflater.from(context);
        mBooks = books;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mBooks.size();
    }

    @Override
    public LVoxBookWrapper getItem(int i) {
        return mBooks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View v = mInflater.inflate(R.layout.grid_item, viewGroup, false);
        ImageView ivThumbnail = (ImageView) v.findViewById(R.id.picture);
        TextView tvTitle = (TextView) v.findViewById(R.id.grid_title);
        TextView tvAuthor = (TextView) v.findViewById(R.id.grid_author);
        final ImageView ivDropDown = (ImageView) v.findViewById(R.id.grid_drop_down_menu);
        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.image_progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        final LVoxBookWrapper book = getItem(i);
        Picasso.with(mContext).load(book.getThumbnailURL()).into(ivThumbnail, new Callback() {
            @Override
            public void onSuccess() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError() {

            }
        });
        tvTitle.setText(book.getTitle());
        tvAuthor.setText(book.getAuthor());
        ivDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, ivDropDown);
                popupMenu.getMenuInflater().inflate(R.menu.browse_books_drop_down_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_download:
                                downloadBook(book);
                                break;
                            /*
                            case R.id.menu_add_to_wishlist:
                                addToWishlist(book);
                                break;
                                */
                            case R.id.menu_listen_to_trailers:
                                broadcastOpenListenToTrailer(book.getId());
                                break;
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        return v;
    }

    private void broadcastOpenListenToTrailer(String urlTitle) {
        Intent intent = new Intent();
        intent.putExtra("URL_TITLE", urlTitle);
        intent.setAction(MainActivity.INTENT_OPEN_LISTEN_TO_TRAILER);
        mContext.sendBroadcast(intent);
    }

    public void updateAdapterWithSearch(String searchString) {
        new SearchTask().execute(searchString);
    }

    private void downloadBook(LVoxBookWrapper book) {
        DownloadManager.getInstance().runDownloadAudiobookTask(mContext, book.getId(), book.getDescription());
    }

    private void addToWishlist(LVoxBookWrapper book) {

    }

    private class SearchTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            mBooks.clear();
            BrowseBooksGridAdapter.this.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(String... params) {
            mBooks = DownloadManager.getInstance().searchTitlesAndAuthors(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            BrowseBooksGridAdapter.this.notifyDataSetChanged();
        }
    }
}
