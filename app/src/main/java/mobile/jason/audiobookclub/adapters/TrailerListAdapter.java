package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Trailer;

/**
 * Created by Demon Pilot on 2015/4/1.
 */
public class TrailerListAdapter extends ArrayAdapter<Trailer> {
    private List<Trailer> mTrailer;
    private MediaPlayer player;
    public TrailerListAdapter(Context context) {
        super(context, R.layout.trailer_list_item);
    }

    @Override
    public int getCount() {
        return mTrailer.size();
    }

    @Override
    public Trailer getItem(int position) {
        return mTrailer.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.trailer_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.trailerName = (TextView) convertView.findViewById(R.id.trailer_name);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.userImage = (ImageView) convertView.findViewById(R.id.user_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Trailer trailer = getItem(position);
        viewHolder.trailerName.setText(trailer.get_trailerName());
        viewHolder.userName.setText(trailer.get_userName());
        byte[] byteArray = trailer.get_userImage();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        viewHolder.userImage.setImageBitmap(bmp);

        return convertView;
    }

    public void setTrailer(List<Trailer> trailer) {
        mTrailer = trailer;
    }

    private static class ViewHolder {
        TextView trailerName;
        TextView userName;
        ImageView userImage;
    }

}
