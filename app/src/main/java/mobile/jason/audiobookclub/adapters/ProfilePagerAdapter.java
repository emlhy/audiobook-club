package mobile.jason.audiobookclub.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import mobile.jason.audiobookclub.Fragments.BadgesGridFragment;
import mobile.jason.audiobookclub.Fragments.LevelFragment;

/**
 * Created by Jason on 3/1/2015.
 */
public class ProfilePagerAdapter extends FragmentPagerAdapter {

    String[] mTabList;
    FragmentManager mManager;
    private FragmentTransaction mCurTransaction = null;

    public ProfilePagerAdapter(FragmentManager fm, String[] tabList) {
        super(fm);
        mManager = fm;
        mTabList = tabList;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new LevelFragment();

            default:
                return new BadgesGridFragment();
        }
    }

    @Override
    public long getItemId(int position) {
        //currently a hack to distinguish My Books tabs vs Profile tabs
        return 1000+position;
    }

    @Override
    public int getCount() {
        return mTabList.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabList[position];
    }

    public void clear(ViewGroup container){
        if (mCurTransaction == null) {
            mCurTransaction = mManager.beginTransaction();
        }

        for(int i = 0; i < getCount(); i++){

            final long itemId = getItemId(i);

            String name = "android:switcher:" + container.getId() + ":" + itemId;
            Fragment fragment = mManager.findFragmentByTag(name);

            if(fragment != null){
                mCurTransaction.detach(fragment);
            }
        }
        mCurTransaction.commitAllowingStateLoss();
        mCurTransaction = null;
    }
}
