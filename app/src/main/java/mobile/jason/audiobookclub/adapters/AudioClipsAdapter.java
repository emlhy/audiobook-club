package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.AudioClip;

/**
 * Created by Demon Pilot on 2015/3/17.
 */
public class AudioClipsAdapter extends ArrayAdapter<AudioClip> {
    private List<AudioClip> mAudioClip;
    private MediaPlayer player;
    public AudioClipsAdapter(Context context) {
        super(context, R.layout.audio_clips_item);
    }

    @Override
    public int getCount() {
        return mAudioClip.size();
    }

    @Override
    public AudioClip getItem(int position) {
        return mAudioClip.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        //OnClick listener;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.audio_clips_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.userName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.audioName = (TextView) convertView.findViewById(R.id.audio_name);
            viewHolder.userImage = (ImageView) convertView.findViewById(R.id.user_image);
            //viewHolder.listen = (Button) convertView.findViewById(R.id.listen_audio_comment);
            //listener = new OnClick();
            //viewHolder.listen.setOnClickListener(listener);
            convertView.setTag(viewHolder);
            //convertView.setTag(viewHolder.listen.getId(), listener);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            //listener = (OnClick) convertView.getTag(viewHolder.listen.getId());
        }

        AudioClip audioClip = getItem(position);
        viewHolder.userName.setText(audioClip.get_userName());
        viewHolder.audioName.setText(audioClip.get_audioName());
        byte[] byteArray = audioClip.get_userImage();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        viewHolder.userImage.setImageBitmap(bmp);
        //listener.setPosition(position);

        return convertView;
    }

    public void setAudioClips(List<AudioClip> audioClip) {
        mAudioClip = audioClip;
    }

    private static class ViewHolder {
        TextView userName;
        TextView audioName;
        ImageView userImage;
        //Button listen;
    }

    /*class OnClick implements View.OnClickListener {
        int position;
        AudioClip audioClip;
        boolean playing = false;

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            audioClip = getItem(position);
            byte[] audioByte = audioClip.get_audioByte();
            if(!playing){
                play(android.os.Environment.getExternalStorageDirectory()+"/audio_comment/" + audioClip.get_audioName() + ".mp3");
                playing = true;
            } else {
                stop();
                playing = false;
            }
        }
    }*/

    /*private void play(String path){
        player = new MediaPlayer();
        try {
            player.reset();
            player.setDataSource(path);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stop(){
        player.stop();
        player.release();
        player = null;
    }*/
}
