package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.activities.MainActivity;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.managers.FilesManager;
import mobile.jason.audiobookclub.managers.ProgressManager;

/**
 * Created by Jason on 3/3/2015.
 */
public final class MyBooksGridAdapter extends BaseAdapter {
    private List<Audiobook> mBooks = new ArrayList<>();
    private final LayoutInflater mInflater;
    private Context mContext;

    public MyBooksGridAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mBooks = FilesManager.getInstance().getMyBooks(context);
        mContext = context;
    }

    @Override
    public int getCount() {
        return mBooks.size();
    }

    @Override
    public Audiobook getItem(int i) {
        return mBooks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mBooks.get(i).get_id();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_item, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.ivThumbnail = (ImageView) convertView.findViewById(R.id.picture);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.grid_title);
            viewHolder.tvAuthor = (TextView) convertView.findViewById(R.id.grid_author);
            viewHolder.ivDropdown = (ImageView) convertView.findViewById(R.id.grid_drop_down_menu);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Audiobook book = getItem(i);

        viewHolder.ivThumbnail.setImageBitmap(book.get_thumbnail());
        viewHolder.tvTitle.setText(book.get_title());
        viewHolder.tvAuthor.setText(book.get_author());
        viewHolder.ivDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolder.ivDropdown);
                popupMenu.getMenuInflater().inflate(R.menu.my_books_drop_down_menu, popupMenu.getMenu());
                if (ProgressManager.getInstance().isBookCompleted(mContext, book)) {
                    popupMenu.getMenu().getItem(0).setEnabled(true);
                }
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_record_trailer:
                                broadcastOpenRecordTrailer(book.get_urlTitle());
                                break;
                            case R.id.menu_listen_to_trailers:
                                broadcastOpenListenToTrailer(book.get_urlTitle());
                                break;
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        return convertView;
    }

    private void broadcastOpenListenToTrailer(String urlTitle) {
        Intent intent = new Intent();
        intent.putExtra("URL_TITLE", urlTitle);
        intent.setAction(MainActivity.INTENT_OPEN_LISTEN_TO_TRAILER);
        mContext.sendBroadcast(intent);
    }

    private void broadcastOpenRecordTrailer(String urlTitle) {
        Intent intent = new Intent();
        intent.putExtra("URL_TITLE", urlTitle);
        intent.setAction(MainActivity.INTENT_OPEN_RECORD_TRAILER);
        mContext.sendBroadcast(intent);
    }

    private static class ViewHolder {
        ImageView ivThumbnail;
        TextView tvTitle;
        TextView tvAuthor;
        ImageView ivDropdown;
    }
}
