package mobile.jason.audiobookclub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Chapter;

/**
 * Created by Jason on 3/3/2015.
 */
public class DrawerAdapter extends ArrayAdapter<String> {

    private String[] mDrawerItems;
    private Context mContext;

    public DrawerAdapter(Context context, String[] drawerItems) {
        super(context, R.layout.drawer_list_item);
        mContext = context;
        mDrawerItems = drawerItems;
    }

    @Override
    public int getCount() {
        return mDrawerItems.length;
    }

    @Override
    public String getItem(int position) {
        return mDrawerItems[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.drawer_list_item, parent, false);
        ImageView icon = (ImageView) v.findViewById(R.id.drawer_item_icon);
        TextView text = (TextView) v.findViewById(R.id.drawer_item_text);

        switch (position) {
            case 0:
                icon.setImageResource(R.drawable.ic_dashboard_grey600_48dp);
                break;
            case 1:
                icon.setImageResource(R.drawable.ic_my_library_books_grey600_48dp);
                break;
            case 2:
                icon.setImageResource(R.drawable.ic_account_box_grey600_48dp);
                break;
            case 3:
                icon.setImageResource(R.drawable.ic_book_grey600_48dp);
                break;
        }

        text.setText(getItem(position));

        return v;
    }
}
