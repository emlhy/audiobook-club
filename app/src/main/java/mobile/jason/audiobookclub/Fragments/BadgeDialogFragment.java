package mobile.jason.audiobookclub.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Badge;

/**
 * Created by Jason on 3/31/2015.
 */
public class BadgeDialogFragment extends DialogFragment {
    private Badge mBadge;

    public BadgeDialogFragment() {

    }

    public BadgeDialogFragment(Badge badge) {
        mBadge = badge;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_badge, null);
        ImageView icon = (ImageView) v.findViewById(R.id.dialog_badge_icon);
        TextView title = (TextView) v.findViewById(R.id.dialog_badge_title);
        TextView encouragement = (TextView) v.findViewById(R.id.dialog_badge_encouragement);
        TextView description = (TextView) v.findViewById(R.id.dialog_badge_description);

        icon.setImageResource(mBadge.get_iconResId());
        title.setText(mBadge.get_badgeName());
        encouragement.setText(mBadge.get_encouragement());
        description.setText(mBadge.get_description());

        builder.setView(v)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        BadgeDialogFragment.this.getDialog().cancel();
                    }
                });
        final AlertDialog diag = builder.create();

        diag.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                diag.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getActivity().getResources().getColor(R.color.primary_color_orange));
            }
        });

        return diag;
    }
}
