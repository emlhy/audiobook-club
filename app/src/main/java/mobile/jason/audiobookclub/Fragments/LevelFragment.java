package mobile.jason.audiobookclub.Fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieSlice;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.managers.ProgressManager;

public class LevelFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_level, container, false);
        PieGraph pieGraph = (PieGraph) v.findViewById(R.id.total_listening_time_pie);
        PieGraph newbiePie = (PieGraph) v.findViewById(R.id.newbie_pie);
        PieGraph novicePie = (PieGraph) v.findViewById(R.id.novice_pie);
        PieGraph scholarPie = (PieGraph) v.findViewById(R.id.scholar_pie);
        TextView totalListeningTime = (TextView) v.findViewById(R.id.total_listening_time_view);
        TextView levelView = (TextView) v.findViewById(R.id.level_view);
        TextView nextLevelupView = (TextView) v.findViewById(R.id.next_levelup_view);

        ProgressManager progressManager = ProgressManager.getInstance();
        totalListeningTime.setText(progressManager.getTotalListeningTimeText(getActivity()));
        ProgressManager.Level level = progressManager.getLevel(getActivity());
        String formatLevel = getActivity().getResources().getString(R.string.format_level);
        levelView.setText(String.format(formatLevel, level.toString()));
        nextLevelupView.setText(progressManager.getNextLevelupText(getActivity(), level));

        setupTotalListeningTimePieGraph(pieGraph);
        setupAllLevelPieGraphs(level, newbiePie, novicePie, scholarPie);

        return v;
    }

    private void setupTotalListeningTimePieGraph(PieGraph pieGraph) {
        ProgressManager progressManager = ProgressManager.getInstance();
        Resources res = getActivity().getResources();
        int darkOrange = res.getColor(R.color.primary_dark_color_orange);
        int orange = res.getColor(R.color.primary_color_orange);
        PieSlice slice = new PieSlice();
        slice.setColor(darkOrange);
        slice.setValue(progressManager.getTotalTime(getActivity()));
        pieGraph.addSlice(slice);
        slice = new PieSlice();
        slice.setColor(orange);
        slice.setValue(progressManager.getNextLevelTime(getActivity()));
        pieGraph.addSlice(slice);
    }

    private void setupAllLevelPieGraphs(ProgressManager.Level level, PieGraph newbiePie, PieGraph novicePie, PieGraph scholarPie) {
        Resources res = getActivity().getResources();
        PieSlice slice = new PieSlice();
        slice.setColor(res.getColor(R.color.grey_pie_chart));
        slice.setValue(1);
        switch (level) {
            case Newbie:
                setupTotalListeningTimePieGraph(newbiePie);
                novicePie.addSlice(slice);
                scholarPie.addSlice(slice);
                break;
            case Novice:
                newbiePie.addSlice(slice);
                setupTotalListeningTimePieGraph(novicePie);
                scholarPie.addSlice(slice);
                break;
            case Scholar:
                newbiePie.addSlice(slice);
                novicePie.addSlice(slice);
                setupTotalListeningTimePieGraph(scholarPie);
                break;
        }
    }
}