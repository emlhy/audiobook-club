package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.ChaptersAdapter;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.entities.Chapter;
import mobile.jason.audiobookclub.entities.LVoxBookWrapper;
import mobile.jason.audiobookclub.managers.DownloadManager;
import mobile.jason.audiobookclub.managers.FilesManager;
import mobile.jason.audiobookclub.managers.ProgressManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookDetailsFragment extends ListFragment {
    private static final String ARG_URL_TITLE = "ARG_URL_TITLE";
    private static final String ARG_FROM_LOCAL = "ARG_FROM_LOCAL";
    private static final String ARG_LVOXBOOK_TITLE = "ARG_LVOXBOOK_TITLE";
    private static final String ARG_LVOXBOOK_AUTHOR = "ARG_ARG_LVOXBOOK_AUTHOR";
    private static final String ARG_LVOXBOOK_DESCRIPTION = "ARG_LVOXBOOK_DESCRIPTION";
    private static final String ARG_LVOXBOOK_COVERARTURL = "ARG_LVOXBOOK_COVERARTURL";
    private static final String ARG_LVOXBOOK_CHAPTERS = "ARG_LVOXBOOK_CHAPTERS";

    private boolean mFromLocal;
    private String mUrlTitle;
    private String mTitle;
    private String mAuthor;
    private String mDescription;
    private Bitmap mCoverArt;
    private String mCoverArtURL;
    private int mNumChapters;
    private ArrayList<Chapter> mChapters;
    private ChaptersAdapter mAdapter;

    private OnFragmentInteractionListener mListener;

    public static BookDetailsFragment newInstance(Audiobook book) {
        BookDetailsFragment fragment = new BookDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL_TITLE, book.get_urlTitle());
        args.putBoolean(ARG_FROM_LOCAL, true);
        fragment.setArguments(args);
        return fragment;
    }

    public static BookDetailsFragment newInstance(LVoxBookWrapper book) {
        BookDetailsFragment fragment = new BookDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL_TITLE, book.getId());
        args.putString(ARG_LVOXBOOK_TITLE, book.getTitle());
        args.putString(ARG_LVOXBOOK_AUTHOR, book.getAuthor());
        args.putString(ARG_LVOXBOOK_DESCRIPTION, book.getDescription());
        args.putString(ARG_LVOXBOOK_COVERARTURL, book.getCoverArtURL());
        args.putInt(ARG_LVOXBOOK_CHAPTERS, book.getChapterNumber());
        args.putBoolean(ARG_FROM_LOCAL, false);
        fragment.setArguments(args);
        return fragment;
    }

    public BookDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mFromLocal = args.getBoolean(ARG_FROM_LOCAL);
            if (mFromLocal) {
                mUrlTitle = getArguments().getString(ARG_URL_TITLE);
                Audiobook audiobook = FilesManager.getInstance().getMyAudiobook(mUrlTitle);
                mTitle = audiobook.get_title();
                mAuthor = audiobook.get_author();
                mDescription = audiobook.get_description();
                mCoverArt = audiobook.get_coverArt();
                mChapters = audiobook.get_chapters();
            } else {
                mUrlTitle = args.getString(ARG_URL_TITLE);
                mTitle = args.getString(ARG_LVOXBOOK_TITLE);
                mAuthor = args.getString(ARG_LVOXBOOK_AUTHOR);
                mDescription = args.getString(ARG_LVOXBOOK_DESCRIPTION);
                mCoverArtURL = args.getString(ARG_LVOXBOOK_COVERARTURL);
                mNumChapters = args.getInt(ARG_LVOXBOOK_CHAPTERS);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView listView = (ListView) inflater.inflate(R.layout.fragment_book_details, container, false);

        View header = inflater.inflate(R.layout.book_details_header, null);
        ImageView bookCoverArt = (ImageView) header.findViewById(R.id.book_picture_image);
        TextView bookTitle = (TextView) header.findViewById(R.id.book_title_text);
        TextView bookAuthor = (TextView) header.findViewById(R.id.book_author_text);
        TextView bookDescription = (TextView) header.findViewById(R.id.book_description_text);
        TextView startListening = (TextView) header.findViewById(R.id.start_listening_text);
        bookTitle.setText(mTitle);
        bookAuthor.setText(mAuthor);
        bookDescription.setText(Html.fromHtml(mDescription));

        if (mFromLocal) {
            bookCoverArt.setImageBitmap(mCoverArt);
            startListening.setText("LISTEN");
            startListening.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onChapterSelected(mUrlTitle, mChapters, mCoverArt, 0);
                }
            });
            mAdapter = new ChaptersAdapter(getActivity(), mChapters, ProgressManager.getInstance().getLastChapterCompleted(getActivity(), mUrlTitle));
        } else {
            final ProgressBar progressBar = (ProgressBar) header.findViewById(R.id.cover_art_process_bar);
            progressBar.setVisibility(View.VISIBLE);
            startListening.setText("DOWNLOAD");
            Picasso.with(getActivity()).load(mCoverArtURL).into(bookCoverArt, new Callback() {
                @Override
                public void onSuccess() {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError() {

                }
            });
            startListening.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DownloadManager.getInstance().runDownloadAudiobookTask(getActivity(), mUrlTitle, mDescription);
                }
            });
            mAdapter = new ChaptersAdapter(getActivity(), mNumChapters);
        }

        setListAdapter(mAdapter);
        listView.addHeaderView(header, null, false);

        setupActionBar();
        return listView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if(position == 0) {
            mListener.onChapterSelected(mUrlTitle, mChapters, mCoverArt, position);
        } else {
            mListener.onChapterSelected(mUrlTitle, mChapters, mCoverArt, position - 1);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onChapterSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setupActionBar() {
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(mTitle);
    }

    public interface OnFragmentInteractionListener {
        public void onChapterSelected(String urlTitle, ArrayList<Chapter> chapters, Bitmap coverArt, int chapterNumber);
    }
}
