package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.ProfilePagerAdapter;

public class ProfileFragment extends Fragment implements ActionBar.TabListener{
        private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProfilePagerAdapter mPagerAdapter;
    private ViewPager mViewPager;
    private ActionBar mActionBar;
    private String mParam1;
    private String mParam2;

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_books, container, false);
        mPagerAdapter = new ProfilePagerAdapter(getActivity().getSupportFragmentManager(), getActivity().getResources().getStringArray(R.array.profile_tab_list));

        mActionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();

        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ((ActionBarActivity)getActivity()).getSupportActionBar().getTabAt(position).select();
            }
        });
        mPagerAdapter.notifyDataSetChanged();

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onPause() {
        removeActionbarTabs();
        mPagerAdapter.clear(mViewPager);
        super.onPause();
    }

    @Override
    public void onResume() {
        setupActionbarTabs();
        mActionBar.setTitle(R.string.my_profile_title);
        mPagerAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    private void setupActionbarTabs() {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            mActionBar.addTab(
                    mActionBar.newTab()
                            .setText(mPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }

    private void removeActionbarTabs() {
        mActionBar.removeAllTabs();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}
