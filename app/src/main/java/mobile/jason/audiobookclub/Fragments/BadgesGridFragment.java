package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.BadgesGridAdapter;
import mobile.jason.audiobookclub.adapters.MyBooksGridAdapter;
import mobile.jason.audiobookclub.entities.Audiobook;

public class BadgesGridFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private BadgesGridAdapter mAdapter;

    public static BadgesGridFragment newInstance(String param1, String param2) {
        BadgesGridFragment fragment = new BadgesGridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public BadgesGridFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_badges_grid, container, false);
        GridView grid = (GridView) v.findViewById(R.id.badges_grid);
        ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.load_progress_bar);
        grid.setEmptyView(progressBar);
        mAdapter = new BadgesGridAdapter(getActivity());
        grid.setAdapter(mAdapter);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }
}
