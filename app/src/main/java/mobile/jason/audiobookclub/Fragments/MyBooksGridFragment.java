package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.MyBooksGridAdapter;
import mobile.jason.audiobookclub.entities.Audiobook;

public class MyBooksGridFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private MyBooksGridAdapter mAdapter;
    private ActionBar mActionBar;

    private OnFragmentInteractionListener mListener;

    public static MyBooksGridFragment newInstance(String param1, String param2) {
        MyBooksGridFragment fragment = new MyBooksGridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MyBooksGridFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_books_grid, container, false);
        GridView grid = (GridView) v.findViewById(R.id.my_books_grid);
        ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.load_progress_bar);
        grid.setEmptyView(progressBar);
        mAdapter = new MyBooksGridAdapter(getActivity());
        grid.setAdapter(mAdapter);

        mActionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onShowBookDetail(mAdapter.getItem(position));
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionBar.setTitle(R.string.my_books_title);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onShowBookDetail(Audiobook book);
    }

}
