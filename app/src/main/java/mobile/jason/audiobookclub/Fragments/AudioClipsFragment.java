package mobile.jason.audiobookclub.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.activities.CommentsActivity;
import mobile.jason.audiobookclub.adapters.AudioClipsAdapter;
import mobile.jason.audiobookclub.database.CommentsSQLiteHelper;
import mobile.jason.audiobookclub.entities.AudioClip;
import mobile.jason.audiobookclub.entities.UserProfile;

/**
 * Created by Demon Pilot on 2015/3/15.
 */
public class AudioClipsFragment extends BaseCommentsFragment {
    private AudioClipsAdapter mAdapter;
    private AudioClip audioClip;
    private List<AudioClip> mAudioClip;
    private UserProfile userProfile;
    private String mUrlTitle;
    private int mChapterNumber;
    private CommentsSQLiteHelper mHelper;
    private MediaPlayer player;
    private boolean playing;

    public AudioClipsFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView listView = (ListView) inflater.inflate(R.layout.fragment_audio_clips, container, false);

        View header = inflater.inflate(R.layout.audio_clips_header, null);
        Button addAudioComment = (Button) header.findViewById(R.id.add_audio_comment);
        ImageView userImage = (ImageView) header.findViewById(R.id.user_image);


        mUrlTitle = super.getUrlTitle();
        mChapterNumber = super.getChapterNumber();
        mAudioClip = new ArrayList<AudioClip>();
        userProfile = new UserProfile();
        playing = false;

        /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/audio_comment");
        if(!folder.exists()){
            folder.mkdir();
        }
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(".mp3")) {
                audioClip = new AudioClip();
                audioClip.set_audioName(listOfFiles[i].getName());
                audioClip.set_path(listOfFiles[i].getAbsolutePath());
                audioClip.set_urlTitle(mUrlTitle);
                audioClip.set_chapterNumber(mChapterNumber);
                mAudioClip.add(audioClip);
            }
        }*/

        addAudioComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastOpenAudioRecorder();
            }
        });

        mHelper = new CommentsSQLiteHelper(getActivity());
        mAudioClip = mHelper.getAudioClips(mUrlTitle, mChapterNumber);
        userProfile = mHelper.getUser(getEmailFromPref());
        byte[] byteArray = userProfile.get_personImage();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        userImage.setImageBitmap(bmp);
        mAdapter = new AudioClipsAdapter(getActivity());
        mAdapter.setAudioClips(mAudioClip);
        //setListAdapter(mAdapter);
        listView.addHeaderView(header);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AudioClip audioClip = mAdapter.getItem(position - 1);
                byte[] audioByte = audioClip.get_audioByte();
                if(!playing){
                    play(android.os.Environment.getExternalStorageDirectory()+"/audio_comment/" + audioClip.get_audioName() + ".mp3");
                    playing = true;
                } else {
                    stop();
                    playing = false;
                }
            }
        });

        return listView;
    }

    private void play(String path){
        player = new MediaPlayer();
        try {
            player.reset();
            player.setDataSource(path);
            player.prepare();
            player.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    private void stop(){
        player.stop();
        player.release();
        player = null;
    }

    private void broadcastOpenAudioRecorder() {
        Intent intent = new Intent();
        intent.setAction(CommentsActivity.INTENT_OPEN_AUDIO_RECORDER);
        getActivity().sendBroadcast(intent);
    }

    private String getEmailFromPref() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return pref.getString("PREF_EMAIL", "");
    }
}
