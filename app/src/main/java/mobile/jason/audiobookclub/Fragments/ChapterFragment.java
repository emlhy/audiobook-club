package mobile.jason.audiobookclub.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.activities.CommentsActivity;
import mobile.jason.audiobookclub.entities.Chapter;
import mobile.jason.audiobookclub.managers.MediaManager;
import mobile.jason.audiobookclub.managers.ProgressManager;

public class ChapterFragment extends Fragment {
    private static final String ARG_URL_TITLE = "ARG_URL_TITLE";
    private static final String ARG_CHAPTERS = "ARG_CHAPTERS";
    private static final String ARG_COVERART = "ARG_COVERART";
    private static final String ARG_CHAPTER_NUMBER = "ARG_CHAPTER_NUMBER";

    private String mUrlTitle;
    private List<Chapter> mChapters;
    private Bitmap mCoverArt;
    private int mChapterNumber;
    private ProgressManager mProgressManager;

    private TextView chapterNum;
    private ImageView cover;
    private SeekBar seekBar;
    private Button previous;
    private Button play;
    private Button next;
    private TextView writeComment;
    private TextView askComment;
    private TextView recordComment;
    private LinearLayout commentsLayout;
    private MediaManager mediaManager;
    private int current;
    private int length;
    private boolean isStartTrackingTouch;
    private Handler handler = new Handler();
    private final MyPlayerListener playerListener = new MyPlayerListener();
    private final MySeekBarListener seekBarListener = new MySeekBarListener();

    private final Runnable seekBarCallback = new Runnable() {
        public void run() {
            if (!isStartTrackingTouch){
                seekBar.setProgress(mediaManager.getCurrentPosition());
            }
            handler.postDelayed(this, 1000);
        }
    };

    public static ChapterFragment newInstance(String urlTitle, ArrayList<Chapter> chapters, Bitmap coverArt, int chapterNumber) {
        ChapterFragment fragment = new ChapterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL_TITLE, urlTitle);
        args.putParcelableArrayList(ARG_CHAPTERS, chapters);
        args.putParcelable(ARG_COVERART, coverArt);
        args.putInt(ARG_CHAPTER_NUMBER, chapterNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ChapterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle args = getArguments();
            mUrlTitle = args.getString(ARG_URL_TITLE);
            mChapters = args.getParcelableArrayList(ARG_CHAPTERS);
            mCoverArt = args.getParcelable(ARG_COVERART);
            mChapterNumber = args.getInt(ARG_CHAPTER_NUMBER);
            current = mChapterNumber;
            mProgressManager = ProgressManager.getInstance();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //ListView listview = (ListView) inflater.inflate(R.layout.fragment_chapter, container, false);
        /*
        mAdapter = new CommentsAdapter(getActivity());
        setListAdapter(mAdapter);
        */

        View header = inflater.inflate(R.layout.fragment_chapter, container, false);
        chapterNum = (TextView) header.findViewById(R.id.chapter_title);
        cover = (ImageView) header.findViewById(R.id.cover);
        seekBar = (SeekBar) header.findViewById(R.id.seek_bar);
        previous = (Button) header.findViewById(R.id.previous);
        play = (Button) header.findViewById(R.id.play_pause);
        next = (Button) header.findViewById(R.id.next);
        writeComment = (TextView) header.findViewById(R.id.write_comment_button);
        askComment = (TextView) header.findViewById(R.id.ask_comment_button);
        recordComment = (TextView) header.findViewById(R.id.record_comment_button);
        commentsLayout = (LinearLayout) header.findViewById(R.id.comments_layout);

        //commentsBanner = (TextView) header.findViewById(R.id.comments_banner);

        updateChapterComments();
        cover.setImageBitmap(mCoverArt);
        chapterNum.setText(mChapters.get(current).get_name());

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous();
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPause();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });

        writeComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getFlagFromPref()){
                    Intent intent = new Intent(getActivity(), CommentsActivity.class);
                    intent.putExtra("FRAG_NUM", 0);
                    intent.putExtra("URL_TITLE", mUrlTitle);
                    intent.putExtra("CHAPTER_NUM", current);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Please login first!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        askComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getFlagFromPref()){
                    Intent intent = new Intent(getActivity(), CommentsActivity.class);
                    intent.putExtra("FRAG_NUM", 1);
                    intent.putExtra("URL_TITLE", mUrlTitle);
                    intent.putExtra("CHAPTER_NUM", current);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Please login first!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        recordComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getFlagFromPref()){
                    Intent intent = new Intent(getActivity(), CommentsActivity.class);
                    intent.putExtra("FRAG_NUM", 2);
                    intent.putExtra("URL_TITLE", mUrlTitle);
                    intent.putExtra("CHAPTER_NUM", current);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Please login first!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //listview.addHeaderView(header);
        //listview.setHeaderDividersEnabled(false);

        //mediaPlayer = MediaManager.getInstance().getMediaPlayer();
        //mediaPlayer.setOnCompletionListener(new MyPlayerListener());
        this.mediaManager = MediaManager.getInstance();
        this.seekBar.setOnSeekBarChangeListener(this.seekBarListener);
        //seekBar.setOnSeekBarChangeListener(new MySeekBarListener());

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
        //getActivity().registerReceiver(new PhoneListener(), filter);
        TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        manager.listen(new MyPhoneStateListener(), PhoneStateListener.LISTEN_CALL_STATE);

        /*header.setFocusable(true);
        header.setFocusableInTouchMode(true);
        header.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    if(event.getAction() == KeyEvent.ACTION_UP){
                        stop();
                    }
                }
                return false;
            }
        } );*/

        return header;
    }

    /*
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        TextView commentBody = (TextView) v.findViewById(R.id.comment_body);
        commentBody.setMaxLines(Integer.MAX_VALUE);
        super.onListItemClick(l, v, position, id);
    }
    */

    private final class MyPhoneStateListener extends PhoneStateListener {
        public void onCallStateChanged(int state, String incomingNumber) {
            pause();
        }
    }

    private final class MyPlayerListener implements MediaPlayer.OnCompletionListener {
        public void onCompletion(MediaPlayer mp) {
            next();
        }
    }

    private void previous() {
        stop();
        isStartTrackingTouch = false;
        current = (current - 1 < 0) ? 0 : current - 1;
        //current = current - 1 < 0 ? mChapters.size() - 1 : current - 1;
        updateChapterComments();
        play();
    }

    private void updateChapterComments() {
        if (mProgressManager.isCommentUnlocked(getActivity(), mUrlTitle, current)) {
            commentsLayout.setVisibility(View.VISIBLE);
        } else {
            commentsLayout.setVisibility(View.GONE);
        }
    }

    private void next() {
        if (!mProgressManager.isCommentUnlocked(getActivity(), mUrlTitle, current)) {
            mProgressManager.setCurrentChapterCompleted(getActivity(), mUrlTitle, current);
            Toast.makeText(getActivity(), mChapters.get(current).get_name() + " discussion unlocked!", Toast.LENGTH_SHORT).show();
        }

        stop();

        if(current + 1 == mChapters.size()){
            seekBar.setProgress(seekBar.getMax());
        } else {
            current = (current + 1 >= mChapters.size()) ? current : current + 1;
            //current = (current + 1) % mChapters.size();
            updateChapterComments();
            play();
        }
    }

    private final class MySeekBarListener implements SeekBar.OnSeekBarChangeListener {

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            isStartTrackingTouch = true;
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            mediaManager.seekTo(seekBar.getProgress());
            isStartTrackingTouch = false;
        }
    }

    private final class MyItemListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            current = position;
            play();
        }
    }

    private void play() {
        try {
            Chapter chapter = mChapters.get(current);
            chapterNum.setText(chapter.get_name());

            this.mediaManager.prepareMediaPlayer();
            this.mediaManager.setDataSource(chapter.get_filePath());
            this.mediaManager.setOnCompletionListener(this.playerListener);
            this.mediaManager.start();

            seekBar.setProgress(0);
            seekBar.setMax(this.mediaManager.getDuration());

            this.syncPlayButton();
            ProgressManager.getInstance().setLastBookListened(getActivity(), mUrlTitle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stop() {
        this.mediaManager.stop();
        isStartTrackingTouch = true;
        this.syncPlayButton();
    }
    private void findAll(File file, List<File> list) {
        File[] subFiles = file.listFiles();
        if (subFiles != null)
            for (File subFile : subFiles) {
                if (subFile.isFile() && subFile.getName().endsWith(".mp3"))
                    list.add(subFile);
                else if (subFile.isDirectory())
                    findAll(subFile, list);
            }
    }

    public void playPause() {
        MediaManager.States state = this.mediaManager.getState();
        switch(state){
            case Uninitialized:
            case Stopped:
                play();
                break;
            case Started:
                pause();
                break;
            case Paused:
                resume();
                break;
            default:
                play();
                break;
        }
    }

    private void resume() {
        //this.mediaManager.seekTo(length);
        this.mediaManager.start();
        this.syncPlayButton();
    }

    private void pause() {
        this.mediaManager.pause();
        //length = this.mediaManager.getCurrentPosition();
        this.syncPlayButton();
    }

    private void syncPlayButton(){
        if(this.mediaManager.isPlaying()){
            play.setText("||");
        }else{
            play.setText("▶");
        }
    }

    private final class PhoneListener extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.handler.post(this.seekBarCallback);
        //resume();
    }

    private boolean getFlagFromPref() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return pref.getBoolean("SIGN_IN", false);
    }

    /*public void onBackPressed() {
        stop();
    }

    @Override
    public void onPause(){
        super.onPause();
        this.handler.removeCallbacks(this.seekBarCallback);
        this.pause();
    }*/
}
