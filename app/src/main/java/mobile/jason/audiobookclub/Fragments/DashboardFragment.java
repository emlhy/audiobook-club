package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieSlice;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.entities.Audiobook;
import mobile.jason.audiobookclub.entities.Badge;
import mobile.jason.audiobookclub.managers.BadgeManager;
import mobile.jason.audiobookclub.managers.FilesManager;
import mobile.jason.audiobookclub.managers.ProgressManager;

public class DashboardFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private ActionBar mActionBar;

    private OnFragmentInteractionListener mListener;

    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ImageView lastReadBookThumbnail = (ImageView) v.findViewById(R.id.picture);
        TextView lastReadBookTitle = (TextView) v.findViewById(R.id.last_read_book_title);
        TextView lastReadBookDurationRemaining = (TextView) v.findViewById(R.id.last_read_book_duration_remaining);
        TextView totalListeningTime = (TextView) v.findViewById(R.id.total_listening_time_view);
        TextView levelView = (TextView) v.findViewById(R.id.level_view);
        TextView nextLevelupView = (TextView) v.findViewById(R.id.next_levelup_view);
        PieGraph pieGraph = (PieGraph) v.findViewById(R.id.dashboard_pie_graph);
        LinearLayout lastBookReadLayout = (LinearLayout) v.findViewById(R.id.last_read_book_layout);

        setupDashboardBadges(v);

        ProgressManager progressManager = ProgressManager.getInstance();
        FilesManager filesManager = FilesManager.getInstance();
        String lastReadUrlTitle = progressManager.getLastBookListened(getActivity());
        final Audiobook book = filesManager.getMyAudiobook(lastReadUrlTitle);

        if (book != null) {
            lastReadBookThumbnail.setImageBitmap(book.get_thumbnail());
            lastReadBookTitle.setText(book.get_title());
            lastReadBookDurationRemaining.setText(getDurationRemainingText(book.get_urlTitle()));
        }

        lastBookReadLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onShowBookDetail(book);
                }
            }
        });

        totalListeningTime.setText(progressManager.getTotalListeningTimeText(getActivity()));
        ProgressManager.Level level = progressManager.getLevel(getActivity());
        String formatLevel = getActivity().getResources().getString(R.string.format_level);
        levelView.setText(String.format(formatLevel, level.toString()));
        nextLevelupView.setText(progressManager.getNextLevelupText(getActivity(),level));

        setupPieGraph(pieGraph);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionBar.setTitle(R.string.dashboard_title);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onShowBookDetail(Audiobook book);
    }

    private String getDurationRemainingText(String urlTitle) {
        int durationRemainingSec = ProgressManager.getInstance().getBookDurationRemaining(getActivity(), urlTitle);
        //rounding errors between metadata seconds vs mediaplayer milliseconds
        if (durationRemainingSec <= 60) {
            return getActivity().getResources().getString(R.string.completed);
        }
        int TotalMin = durationRemainingSec/60;
        int hr = TotalMin/60;
        int min = TotalMin%60;
        String str = getActivity().getResources().getString(R.string.format_remaining_duration);

        return String.format(str, hr, min);
    }

    private void setupPieGraph(PieGraph pieGraph) {
        ProgressManager progressManager = ProgressManager.getInstance();
        Resources res = getActivity().getResources();
        int darkOrange = res.getColor(R.color.primary_dark_color_orange);
        int orange = res.getColor(R.color.primary_color_orange);
        PieSlice slice = new PieSlice();
        slice.setColor(darkOrange);
        slice.setValue(progressManager.getTotalTime(getActivity()));
        pieGraph.addSlice(slice);
        slice = new PieSlice();
        slice.setColor(orange);
        slice.setValue(progressManager.getNextLevelTime(getActivity()));
        pieGraph.addSlice(slice);
    }

    private void setupDashboardBadges(View v) {
        LinearLayout badgeLayout1 = (LinearLayout) v.findViewById(R.id.dashboard_badge_layout1);
        LinearLayout badgeLayout2 = (LinearLayout) v.findViewById(R.id.dashboard_badge_layout2);
        LinearLayout badgeLayout3 = (LinearLayout) v.findViewById(R.id.dashboard_badge_layout3);
        LinearLayout badgeLayout4 = (LinearLayout) v.findViewById(R.id.dashboard_badge_layout4);
        LinearLayout badgeLayout5 = (LinearLayout) v.findViewById(R.id.dashboard_badge_layout5);
        ImageView badgeIcon1 = (ImageView) v.findViewById(R.id.dashboard_badge1);
        ImageView badgeIcon2 = (ImageView) v.findViewById(R.id.dashboard_badge2);
        ImageView badgeIcon3 = (ImageView) v.findViewById(R.id.dashboard_badge3);
        ImageView badgeIcon4 = (ImageView) v.findViewById(R.id.dashboard_badge4);
        ImageView badgeIcon5 = (ImageView) v.findViewById(R.id.dashboard_badge5);
        TextView badgeTitle1 = (TextView) v.findViewById(R.id.dashboard_badge1_title);
        TextView badgeTitle2 = (TextView) v.findViewById(R.id.dashboard_badge2_title);
        TextView badgeTitle3 = (TextView) v.findViewById(R.id.dashboard_badge3_title);
        TextView badgeTitle4 = (TextView) v.findViewById(R.id.dashboard_badge4_title);
        TextView badgeTitle5 = (TextView) v.findViewById(R.id.dashboard_badge5_title);

        HashMap<Integer, LinearLayout> layoutMap = new HashMap<>();
        layoutMap.put(1, badgeLayout1);
        layoutMap.put(2, badgeLayout2);
        layoutMap.put(3, badgeLayout3);
        layoutMap.put(4, badgeLayout4);
        layoutMap.put(5, badgeLayout5);

        HashMap<Integer, ImageView> iconMap = new HashMap<>();
        iconMap.put(1, badgeIcon1);
        iconMap.put(2, badgeIcon2);
        iconMap.put(3, badgeIcon3);
        iconMap.put(4, badgeIcon4);
        iconMap.put(5, badgeIcon5);

        HashMap<Integer, TextView> titleMap = new HashMap<>();
        titleMap.put(1, badgeTitle1);
        titleMap.put(2, badgeTitle2);
        titleMap.put(3, badgeTitle3);
        titleMap.put(4, badgeTitle4);
        titleMap.put(5, badgeTitle5);

        BadgeManager badgeManager = BadgeManager.getInstance();
        List<Badge> badges = BadgeManager.getInstance().getBadges();
        int viewIndex = 5;

        for (Badge badge : badges) {
            if (badgeManager.isBadgeUnlocked(getActivity(), badge)) {
                layoutMap.get(viewIndex).setVisibility(View.VISIBLE);
                iconMap.get(viewIndex).setImageResource(badge.get_iconResId());
                titleMap.get(viewIndex).setText(badge.get_badgeName());
                viewIndex--;
                if (viewIndex <= 0) {
                    break;
                }
            }
        }
    }
}
