package mobile.jason.audiobookclub.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.jason.audiobookclub.R;

/**
 * Created by Demon Pilot on 2015/3/15.
 */
public class QAFragment extends BaseCommentsFragment {
    public QAFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_qa, container, false);
        return rootView;
    }
}
