package mobile.jason.audiobookclub.Fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.BrowseBooksGridAdapter;
import mobile.jason.audiobookclub.entities.LVoxBookWrapper;
import mobile.jason.audiobookclub.managers.DownloadManager;

public class BrowseBooksGridFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private BrowseBooksGridAdapter mAdapter;
    private ActionBar mActionBar;
    private OnFragmentInteractionListener mListener;
    private List<LVoxBookWrapper> mBooks;

    public static BrowseBooksGridFragment newInstance(String param1, String param2) {
        BrowseBooksGridFragment fragment = new BrowseBooksGridFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public BrowseBooksGridFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_my_books_grid, container, false);

        mActionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        GridView grid = (GridView) v.findViewById(R.id.my_books_grid);
        ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.load_progress_bar);
        grid.setEmptyView(progressBar);

        mBooks = DownloadManager.getInstance().getLatestBooks();
        mAdapter = new BrowseBooksGridAdapter(getActivity(), mBooks);
        grid.setAdapter(mAdapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onShowBookDetail(mAdapter.getItem(position));
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActionBar.setTitle(R.string.browse_books_title);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.searchview_in_menu, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mAdapter.updateAdapterWithSearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onShowBookDetail(LVoxBookWrapper book);
    }
}
