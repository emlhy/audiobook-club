package mobile.jason.audiobookclub.Fragments;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.adapters.CommentsAdapter;
import mobile.jason.audiobookclub.database.CommentsSQLiteHelper;
import mobile.jason.audiobookclub.entities.Comment;
import mobile.jason.audiobookclub.entities.UserProfile;

/**
 * Created by Demon Pilot on 2015/3/15.
 */
public class CommentsFragment extends BaseCommentsFragment {
    private CommentsAdapter mAdapter;
    private CommentsSQLiteHelper mHelper;
    private UserProfile userProfile;
    private String mUrlTitle;
    private int mChapterNumber;
    private List<Comment> mComments;

    public CommentsFragment(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ListView listView = (ListView) inflater.inflate(R.layout.fragment_comments, container, false);

        View header = inflater.inflate(R.layout.comment_header, null);
        final EditText textComment = (EditText) header.findViewById(R.id.body_comment);
        ImageView userImage = (ImageView) header.findViewById(R.id.user_image);
        mUrlTitle = super.getUrlTitle();
        mChapterNumber = super.getChapterNumber();
        userProfile = new UserProfile();

        mHelper = new CommentsSQLiteHelper(getActivity());
        mComments = mHelper.getComments(mUrlTitle, mChapterNumber);
        userProfile = mHelper.getUser(getEmailFromPref());
        byte[] byteArray = userProfile.get_personImage();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        userImage.setImageBitmap(bmp);

        mAdapter = new CommentsAdapter(getActivity());
        mAdapter.setComments(mComments);
        listView.addHeaderView(header);
        listView.setAdapter(mAdapter);

        textComment.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    String bodyText = textComment.getText().toString();
                    if(bodyText.isEmpty()) {
                        Toast.makeText(getActivity(), "Comment cannot be empty!", Toast.LENGTH_SHORT).show();
                        return false;
                    } else {
                        textComment.setText("");
                        mHelper.addComment(new Comment(mUrlTitle, mChapterNumber, bodyText, userProfile.get_personName(), userProfile.get_personImage()));
                        mComments = mHelper.getComments(mUrlTitle, mChapterNumber);
                        mAdapter.setComments(mComments);
                    }
                    return false;
                }
                return false;
            }
        });
        return listView;
    }

    private String getEmailFromPref() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return pref.getString("PREF_EMAIL", "");
    }
}
