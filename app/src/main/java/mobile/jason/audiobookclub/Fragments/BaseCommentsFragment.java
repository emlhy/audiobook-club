package mobile.jason.audiobookclub.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Demon Pilot on 2015/3/24.
 */
public abstract class BaseCommentsFragment extends Fragment{
    public String mUrlTitle;
    public int mChapterNumber;

    public BaseCommentsFragment(){

    }

    public String getUrlTitle(){
        if (getArguments() != null) {
            Bundle args = getArguments();
            mUrlTitle = args.getString("URL_TITLE");
        }
        return mUrlTitle;
    }

    public int getChapterNumber(){
        if (getArguments() != null) {
            Bundle args = getArguments();
            mChapterNumber = args.getInt("CHAPTER_NUM");
        }
        return mChapterNumber;
    }
}
