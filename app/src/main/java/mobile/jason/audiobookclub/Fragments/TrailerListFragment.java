package mobile.jason.audiobookclub.Fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.activities.CommentsActivity;
import mobile.jason.audiobookclub.adapters.AudioClipsAdapter;
import mobile.jason.audiobookclub.adapters.TrailerListAdapter;
import mobile.jason.audiobookclub.database.CommentsSQLiteHelper;
import mobile.jason.audiobookclub.entities.AudioClip;
import mobile.jason.audiobookclub.entities.Trailer;

/**
 * Created by Demon Pilot on 2015/4/1.
 */
public class TrailerListFragment extends Fragment {
    private TrailerListAdapter mAdapter;
    private Trailer trailer;
    private List<Trailer> mTrailer;
    private String mUrlTitle;
    private CommentsSQLiteHelper mHelper;
    private MediaPlayer player;
    private boolean playing;

    public TrailerListFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView rootView = (ListView) inflater.inflate(R.layout.fragment_trailer, container, false);

        mUrlTitle = getArguments().getString("URL_TITLE");
        mTrailer = new ArrayList<Trailer>();
        playing = false;

        mHelper = new CommentsSQLiteHelper(getActivity());
        mTrailer = mHelper.getTrailer(mUrlTitle);
        mAdapter = new TrailerListAdapter(getActivity());
        mAdapter.setTrailer(mTrailer);
        //setListAdapter(mAdapter);
        rootView.setAdapter(mAdapter);
        rootView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trailer trailer = mAdapter.getItem(position);
                byte[] audioByte = trailer.get_trailerByte();
                if(!playing){
                    play(android.os.Environment.getExternalStorageDirectory()+"/audio_comment/" + trailer.get_trailerName() + ".mp3");
                    playing = true;
                } else {
                    stop();
                    playing = false;
                }
            }
        });

        return rootView;
    }

    private void play(String path){
        player = new MediaPlayer();
        try {
            player.reset();
            player.setDataSource(path);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stop(){
        player.stop();
        player.release();
        player = null;
    }
}
