package mobile.jason.audiobookclub.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import mobile.jason.audiobookclub.R;
import mobile.jason.audiobookclub.database.CommentsSQLiteHelper;
import mobile.jason.audiobookclub.entities.AudioClip;
import mobile.jason.audiobookclub.entities.Comment;
import mobile.jason.audiobookclub.entities.UserProfile;
import mobile.jason.audiobookclub.managers.AudioManager;
import mobile.jason.audiobookclub.managers.ProgressManager;

/**
 * Created by Demon Pilot on 2015/3/17.
 */
public class AudioRecorderFragment extends BaseCommentsFragment {
    private ImageView volume;
    private Chronometer timedown;
    private View microphonePopup;
    private Button record;
    private Button done;
    private EditText commentName;
    private LinearLayout microphoneLayout;
    private String name;
    private AudioManager audioManager;
    private Handler mHandler;
    private String mUrlTitle;
    private int mChapterNumber;
    private CommentsSQLiteHelper mHelper;
    private AudioClip audioClip;
    private static final int POLL_INTERVAL = 300;
    private long startVoiceTime, endVoiceTime;
    private long timeTotal = 0;
    private long timeLeft = 0;
    private int flag;
    private UserProfile userProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_audio_recorder, container, false);

        commentName = (EditText) rootView.findViewById(R.id.comment_name);
        record = (Button) rootView.findViewById(R.id.record);
        done = (Button) rootView.findViewById(R.id.done);
        microphonePopup = rootView.findViewById(R.id.microphone_popup);
        volume = (ImageView) rootView.findViewById(R.id.volume);
        timedown=(Chronometer) rootView.findViewById(R.id.timedown);
        microphoneLayout = (LinearLayout) rootView.findViewById(R.id.microphone_layout);

        mUrlTitle = super.getUrlTitle();
        mChapterNumber = super.getChapterNumber();
        userProfile = new UserProfile();
        mHelper = new CommentsSQLiteHelper(getActivity());
        userProfile = mHelper.getUser(getEmailFromPref());

        mHandler = new Handler();
        flag = 1;
        audioManager = new AudioManager();

        commentName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(commentName.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                    return true;
                }
                return false;
            }
        });

        record.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    name = commentName.getText().toString();
                    if(name.isEmpty()) {
                        Toast.makeText(getActivity(), "Name cannot be empty!", Toast.LENGTH_SHORT).show();
                        return false;
                    } else {
                        record.setBackgroundColor(getResources().getColor(R.color.primary_dark_color_orange));
                        record.setText("Release to stop");
                        record.setTextColor(getResources().getColor(R.color.common_white));
                        int[] location = new int[2];
                        record.getLocationInWindow(location);
                        int btn_rc_Y = location[1];
                        int btn_rc_X = location[0];
                        if (flag == 1) {
                            if (!Environment.getExternalStorageDirectory().exists()) {
                                Toast.makeText(getActivity(), "No SDCard", Toast.LENGTH_LONG).show();
                                return false;
                            }
                            //if (event.getY() < btn_rc_Y && event.getX() > btn_rc_X) {
                            microphonePopup.setVisibility(View.VISIBLE);
                            mHandler.postDelayed(new Runnable() {
                                public void run() {
                                }
                            }, 300);
                            startVoiceTime = SystemClock.currentThreadTimeMillis();
                            start(name + ".mp3");
                            timedown.setVisibility(View.VISIBLE);
                            initTimer(30);
                            timedown.start();
                            flag = 2;
                            //}
                        }
                    }
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    record.setBackgroundColor(getResources().getColor(R.color.common_white));
                    record.setText("Hold to Record");
                    record.setTextColor(Color.BLACK);
                    timedown.stop();
                    if (flag == 2) {
                        microphonePopup.setVisibility(View.GONE);
                        timedown.setVisibility(View.GONE);
                        stop();
                        flag = 1;
                    } else {
                        microphoneLayout.setVisibility(View.GONE);
                        stop();
                        endVoiceTime = SystemClock.currentThreadTimeMillis();
                        flag = 1;
                    }
                }
                return false;
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressManager.getInstance().addAudioCommentMade(getActivity());
                getActivity().onBackPressed();
            }
        });
        return rootView;
    }


    private void start(String name) {
        audioManager.start(name);
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        byte[] audioByte = audioManager.getAudioInByte(android.os.Environment.getExternalStorageDirectory()+"/audio_comment/" + name + ".mp3");
        audioClip = new AudioClip(mUrlTitle, mChapterNumber, name, audioByte, userProfile.get_personName(), userProfile.get_personImage());
        mHelper.addAudioClip(audioClip);
        audioManager.stop();
        volume.setImageResource(R.drawable.amp1);
    }

    private Runnable mSleepTask = new Runnable() {
        public void run() {
            stop();
        }
    };
    private Runnable mPollTask = new Runnable(){
        public void run() {
            double amp = audioManager.getAmplitude();
            updateDisplay(amp);
            mHandler.postDelayed(mPollTask, POLL_INTERVAL);
        }
    };

    private void updateDisplay(double signalEMA) {
        switch ((int) signalEMA) {
            case 0:
            case 1:
                volume.setImageResource(R.drawable.amp1);
                break;
            case 2:
            case 3:
                volume.setImageResource(R.drawable.amp2);
                break;
            case 4:
            case 5:
                volume.setImageResource(R.drawable.amp3);
                break;
            case 6:
            case 7:
                volume.setImageResource(R.drawable.amp4);
                break;
            case 8:
            case 9:
                volume.setImageResource(R.drawable.amp5);
                break;
            case 10:
            case 11:
                volume.setImageResource(R.drawable.amp6);
                break;
            default:
                volume.setImageResource(R.drawable.amp7);
                break;
        }
    }

    private void initTimer(long total) {
        this.timeTotal = total;
        this.timeLeft = total;
        timedown.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if (timeLeft <= 0) {
                    Toast.makeText(getActivity(), "Time is up", Toast.LENGTH_SHORT).show();
                    timedown.stop();
                    stop();
                    microphonePopup.setVisibility(View.GONE);
                    timedown.setVisibility(View.GONE);
                    return;
                }
                timeLeft--;
                refreshTimeLeft();
            }
        });
    }
    private void refreshTimeLeft() {
        this.timedown.setText("Time left：" + timeLeft);
    }


    private String getEmailFromPref() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return pref.getString("PREF_EMAIL", "");
    }

}
